Utilisation de l'application Bookmarks
======================================

Si vous voulez ajouter un marque-page à l'application Bookmarks, 
vous pouvez utiliser l'interface principale ou le marque-page automatisé.

L'interface principale
----------------------

Ajout d'un marque-page
~~~~~~~~~~~~~~~~~~~~~~
Dans l'application Bookmarks, saisir une URL dans la zone supérieure gauche de la section de contenu. Après avoir ajouter une adresse,
cliquer sur le bouton représentant un crayon pour modifier les champs cencernant cette adresse.
L'interface principake de marque-page d'ownCloud bookmark contient trois champs où peuvent
être saisis l'adresse du site Web (ou URL), le titre du marque-page et un
ensemble d'étiquettes séparées par des espaces.

.. figure:: images/bookmark_addurl.png

	Ajout manuel d'un marque-page

Dans cet exemple, la page `http://wikipedia.org` a été ajoutée avec le titre « Wikipedia »
et quelques étiquettes décrivant à quoi sert Wikipedia pour des recherches ultérieures plus faciles.

Modification/suppression d'un marque-page
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vous avez aussi la possibilité de modifier ou supprimer un marque-page.

Pour modifier un marque-page, passer la souris sur le marque-page et cliquer sur l'icône représentant un crayon. 
Les détails du marque-page seront alors affichés dans trois champs en haut de l'écran.
Modifiez le marque-page selon vos besoins puis cliquez sur le bouton d'enregistrement pour conserver les modifications.

Pour supprimer un marque-page, passer la souris sur celui-ci et cliquer sur l'icône représentant une croix.

Recherche
~~~~~~~~~

Si vous cliquez sur une étiquette, ownCloud n'affichera que les marque-pages décrits par
cette étiquette.

Vous pouvez également utiliser la barre de recherche d'ownCloud sur le coin supérieur droit de l'écran.

Cliquer sur le menu « Bookmarks » de la barre latérale pour revenir à la vue par défaut.


Le marque-page automatisé
-------------------------

.. figure:: images/bookmark_setting.png

	Lien Bookmarklet

Le créateur de cette application comprend que les utilisateurs ne veulent pas ouvrir
la page de marque-pages d'ownCloud pour ajouter un marque-page chaque fois q'ils voient un site intéressant.
C'est pourquoi il a créé ce « bookmarklet ».

Un bookmarklet est un petit bouton que vous pouvez glisser et déposer dans vos marque-pages.
La prochaine fois que vous voyez un site intéressant, cliquez sur ce marque-page spécial
pour ajouter ce site à vos marque-pages ownCloud.

Pour trouver ce marque-page, cliquer sur l'icône représentant un engrenage au bas de l'application Bookmarks.


