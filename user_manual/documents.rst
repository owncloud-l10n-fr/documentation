Édition collaborative de documents
==================================

L'application Documents gère l'édition de documents dans ownCloud, sans avoir 
à lancer une application externe. L'application Documents propose les fonctionnalités
suivantes :

* édition colloborative de documents, avec plusieurs utilisateurs éditant les fichiers simultanément ; 
* création de document dans ownCloud ;
* téléversement de document ;
* partage et édition de fichiers dans le navigateur, puis partage dans ownCloud ou 
  par un lien public.

Les formats de fichiers gérés sont : `.odt`, `.doc` et `.docx`.

L'interface principale
----------------------

.. image:: images/oc_documents.png

Création ou téléversement d'un document
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans l'application Documents, vous pouvez téléverser un document existant ou en créer 
un nouveau. Le bouton *Nouveau document* crée un document libellé « Nouveau 
document.odt ». L'extension ODT est un format OpenDocument, qui est géré par de nombreux 
logiciels de traitement de texte dont Microsoft Word, LibreOffice Writer et 
OpenOffice Writer.

Modification d'un document
~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour modifier un document, accéder à l'application Documents à partir du menu Applications dans le coin
supérieur gauche de la fenêtre ownCloud. 

.. image:: images/oc_documents_edit.png

#. Cliquer sur le nom de fichier pour le modifier.
#. Partager le document (consulter la section :ref:`Partage de document 
   <share-a-document>`).
#. Barre d'outils de formatage.
#. Zoom.
#. Fermeture et enregistrement.
#. Utilisateurs en train d'éditer le document.

Édition collaborative d'un document
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour modifier un document de manière collaborative, il doit être partagé avec tout le monde et nécessite des
permissions de modification. Plusieurs utilisateurs peuvent le modifier en même temps, et les modification 
apparaissent en temps réel. Le curseur de chaque utilisateur est de la même couleur que celle qui borde 
leur image utilisateur.

Si un utilisateur n'est pas un utilisateur local (c'est-à-dire qu'il accède au document à partir d'un lien public), il
sera affiché en tant qu'invité dans la liste des utilisateurs, nommé automatiquement Invité 1, Invité 2, 
et ainsi de suite. Les invités peuvent changer leur pseudonymes à tout moment en cliquant sur leur
nom ou leur imagette dans la liste des utilisateurs.

Suppression d'un document
~~~~~~~~~~~~~~~~~~~~~~~~~

Un document ne peut être supprimé à partir de l'application Documents, seulement dans la page
Fichiers d'ownCloud. Elle se trouve dans le répertoire par défaut des documents 
qui est configuré dans la page Personnel d'ownCloud (consulter 
:doc:`userpreferences`).

.. _share-a-document:

Partage d'un document
~~~~~~~~~~~~~~~~~~~~~

Le partage d'un document dispose des mêmes options que le partage des autres fichiers. Lors de l'édition d'un
document, vous pouvez utiliser le bouton *Partager* pour permettre à d'autres utilisateurs de modifier le
document. Ce bouton affichera toutes les options disponibles.

