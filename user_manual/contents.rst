.. _contents:

==================
Table des matières
==================

.. toctree::
    :maxdepth: 2
    
    index
    whats_new
    webinterface
    files/index
    pim/index
    documents
    userpreferences
    external_storage/index
    
..  removed from 9?    
..  bookmarks    

