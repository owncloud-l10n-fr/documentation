=================================
Configuration du stockage externe
=================================

L'application de stockage externe permet de configurer l'accès au services de stockage externes tels que
Google Drive, Dropbox, Amazon S3, les serveurs de fichiers SMB/CIFS et les serveurs FTP dans 
ownCloud. Votre administrateur de serveur ownCloud contrôle la disponibilité des ces
services. Veuillez consulter `Configuration du stockage externe 
:doc:`admin_manual/configuration_files/external_storage_configuration_gui`
dans le manuel administrateur d'ownCloud pour des exemples de configuration.
