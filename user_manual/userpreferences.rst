=======================
Préférences utilisateur
=======================

Vous pouvez gérer vos paramètres personnels.

Pour accéder aux préférences utilisateur :

1. Cliquer sur votre nom d'utilisateur dans le coin supérieur droit de la fenêtre ownCloud.

   Le menu des paramètres personnel s'ouvre.

   .. figure:: images/oc_personal_settings_dropdown.png
      :alt: Copie d'écran du menu utilisateur de l'interface Web d'ownCloud

   *Menu des paramètres personnels*

2. Sélectionner *Personnel* dans la liste déroulante.

   .. figure:: images/personal_settings.png
      :alt: Copie d'écran de la page des paramètres personnels

.. note:: Si vous êtes administrateur, vous pouvez aussi gérer et administrer
   le serveur. Ces liens n'apparaissent pour les utilisateurs qui ne sont pas administrateurs.

Les options présentes dans la page des paramètres personnels dépendent des applications qui ont 
été activées par l'administrateur. Certaines des fonctionnalités que vous verrez sont les
suivantes :

* utilisation et quota disponible ;
* gestion de la photo du profil ;
* nom complet. Vous pouvez indiquer ce que vous voulez, car ceci est indépendant de votre nom de connexion 
  à ownCloud, qui est unique et ne peut être modifié ;
* adresse électronique ;
* appartenance aux groupes ;
* gestion du mot de passe ;
* langue de l'interface d'ownCloud.
* liens vers les applications pour ordinateurs et mobiles ;
* gestion de l'activité et des notifications ;
* répertoire par défaut où enregistrer les nouveaux documents ;
* identifiant de partage fédéré ;
* liens de partage de réseaux sociaux ;
* version d'ownCloud.
