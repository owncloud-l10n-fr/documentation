========================
Interface Web d'ownCloud
========================

Vous pouvez vous connecter au serveur ownCloud en utilisant n'importe quel navigateur Web ; il suffit 
d'indiquer l'adresse du serveur ownCloud et de saisir vos nom d'utilisateur et mot de passe. Les navigateurs 
gérés sont :

* Firefox 14+
* Chrome 18+
* Safari 5+
* IE9+ (sauf le mode de compatibilité)

  .. figure:: images/oc_connect.png
     :alt: Écran de connexion d'ownCloud.

.. note:: Certaines applications comme ``files_external`` ou ``encryption`` désactiveront 
   la case à cocher **Rester connecté** .

L'interface Web principale
--------------------------

Par défaut, l'interface Web d'ownCloud s'ouvre sur la page Fichiers. Vous pouvez ajouter, 
supprimer, partager des fichiers et faire des modifications en fonction des permissions que vous avez définies 
(si vous êtes administrateur du serveur) ou définies par votre administrateur.

.. figure:: images/files_page.png
     :scale: 75%
     :alt: La vue principale Fichiers.

L'interface utilisateur d'ownCloud contient les fonctions et champs suivants :

* **Le menu de sélection des applications** : Situé dans le coin supérieur gauche de la fenêtre. Cliquer sur la flèche 
  pour ouvrir une liste déroulante dévoilant les différentes applications disponibles.
  
* **Les champs d'information sur les applications** : Situés dans la barre latérale gauche, ils proposent 
  différents filtres et tâches relatifs à l'application sélectionnée. Par exemple, pour
  l'application Fichiers, un ensemble de filtres permet de trouver rapidement des fichiers, tels que
  les fichiers qui ont été partagés avec vous ou que vous partagez avec d'autres utilisateurs.
  Différentes options sont disponibles pour les autres applications.

* **La vue application** : Le champ central principal dans l'interface utilisateur d'ownCloud.
  Ce champ affiche le contenu ou les fonctionnalités utilisateur de l'application sélectionnée.

* **La barre de navigation** : Située au-dessus de la fenêtre d'affichage principale (la vue application).
  Cette barre propose un fil d'Ariane qui permet de naviguer vers les niveaux supérieurs de la hiérarchie
  de dossiers, jusqu'au niveau racine.

* **Le bouton nouveau** : Situé dans la barre de navigation (icône « + »). Il permet de créer de nouveaux
  fichiers ou dossiers ou de téléverser des fichiers.

.. note:: Vous pouvez aussi glisser et déposer des fichiers à partir de votre gestionnaire de fichiers
   vers l'application Fichiers d'ownCloud pour les téléverser vers le serveur ownCloud. Actuellement, cette
   fonctionnalité n'est disponible que pour les navigateurs Chrome et 
   Chromium.

* **Champ de recherche** : Cliquer sur l'icône représentant une loupe dans le coin supérieur droit de la fenêtre
  pour rechercher des fichiers.
  
* **Le bouton galerie** : Cliquer sur l'icône représentant quatre petits carrés dans le coin supérieur droit
  de la fenêtre, pour être amené directement dans la galerie d'images.

* **Le menu des paramètres personnels** : Cliquez sur votre nom d'utilisateur ownCloud, situé à droite 
  du bouton de recherche pour ouvrir la liste déroulante des paramètres personnels. La page personnelle 
  contient les fonctionnalités et paramètres suivants :

  * liens de téléchargement vers les applications pour ordinateurs et mobiles ;
  * lien pour relancer l'assistant de premier démarrage ;
  * utilisation et espace de stockage disponible ;
  * gestion du mot de passe ;
  * nom, adresse électronique et photo du profil ;
  * appartenance aux groupes ;
  * langue de l'interface ;
  * gestion des notifications ;
  * identifiant au sein du nuage fédéré ;
  * boutons de partage sur les réseaux sociaux ;
  * gestion de certificat SSL ;
  * information de version d'ownCloud.

Consulter la section :doc:`userpreferences` pour en apprendre plus sur ces paramètres.
