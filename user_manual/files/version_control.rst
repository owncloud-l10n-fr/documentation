===================
Contrôle de version
===================

ownCloud propose un système de contrôle de version de fichiers simple. Le système
crée des sauvegardes des fichiers qui sont accessibles dans l'onglet Versions
dans la barre latérale de détails. Cet onglet contient l'historique du fichier dans
lequel vous pouvez remonter à n'importe quelle version de celui-ci. Les modifications
effectuées à des intervalles supérieurs à deux minutes sont stockés dans data/[utilisateur]/versions.

.. figure:: ../images/files_versioning.png

Pour restaurer une version spécifique d'un fichier, cliquer sur la flèche circulaire à gauche.
Cliquer sur l'horodatage pour le télécharger.

Le système de gestion de version supprime les anciennes versions de fichiers pour s'assurer que l'utilisateur
ne soit pas à court d'espace disque. Le processus suivant est utilisé pour supprimer les anciennes
versions :

* la première seconde, ownCloud conserve une version ;
* les 10 premières secondes, ownCloud conserve une version toutes les 2 secondes ;
* la première minute, ownCloud conserve une version toutes les 10 secondes ;
* la première heure, ownCloud conserve une version par minute ;
* les premières 24 heures, ownCloud conserve une version par heure ;
* les 30 premiers jours, ownCloud conserve une version par jour ;
* après les 30 premiers jours, ownCloud conserve une version par semaine.

Les versions sont ajustées selon ce processus chaque fois qu'une nouvelle version
est créée.

L'application de gestion de version n'utilise jamais plus de 50% de l'espace disque libre de l'utilisateur.
Si les versions stockées excèdent cette limite, ownCloud supprime les plus anciennes versions jusqu'à tomber
à nouveau sous la limite des 50%.
