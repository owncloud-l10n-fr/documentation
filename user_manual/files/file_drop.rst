=======================
Téléversements anonymes
=======================

**Cette option n'est disponible que pour la Souscription Entreprise.**
Si votre administrateur ownCloud a activé l'application
Files Drop, vous pouvez créer un répertoire de téléversement spécial de sorte que d'autres
utilisateurs puissent téléverser des fichiers sans avoir besoin de s'authentifier sur le serveur ou être 
un utilisateur d'ownCloud. Ils ne seront pas autorisés à voir le contenu de ce
répertoire ou d'y faire des modifications. C'est une excellente alternative à l'envoi de pièces jointes
volumineuses par courriel, à l'utilisation de serveurs FTP ou à l'utilisation de services commerciaux de
partage de fichiers.

Paramétrage de l'application File Drop
--------------------------------------

Rendez-vous sur votre page personnelle dans la section de configuration de Files Drop.

.. image:: ../images/files-drop-2.png

Cliquer sur le bouton **Choisir** pour ouvrir une boîte de dialogue permettant de sélectionner le répertoire
de téléversement. Vous pourriez d'abord souhaiter de créer un répertoire dédié pour le téléversement
(sur votre page Fichiers), nommée **upload** dans l'exemple suivant :

.. figure:: ../images/files-drop-3.png
   :scale: 50% 
   
   *Cliquer pour agrandir l'image*
   
Sur votre page personnelle, vous devriez à présent voir une URL pour le répertoire de téléversement. Partagez cette
URL avec vos correspondant pour les autoriser à téléverser dans votre répertoire Files Drop. Dans cet exemple, la taille 
maximale de téléversement est de 512 Mo. Ceci est paramétrable. Contactez votre administrateur ownCloud si vous avez besoin
d'une taille plus importante.

.. image:: ../images/files-drop-4.png

Téléversements de fichiers
---------------------------

L'utilisation de l'application Files Drop est simple. Vous recevez un lien vers le répertoire de téléversement.
Cliquer sur ce lien ouvrira une page ownCloud avec un bouton **Cliquez pour téléverser**.

.. figure:: ../images/files-drop-5.png
   :scale: 50% 

   *Cliquer pour agrandir l'image*
   
Ce bouton ouvre une interface de sélection de fichier permettant de sélectionner le fichier ou le
répertoire que vous souhaitez téléverser.

.. figure:: ../images/files-drop-6.png
   :scale: 50% 

   *Cliquer pour agrandir l'image*
   
Quand le téléversement est terminé, un message de confirmation s'affiche indiquant le nom des fichiers.

.. figure:: ../images/files-drop-7.png
