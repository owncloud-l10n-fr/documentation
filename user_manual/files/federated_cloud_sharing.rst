===============================
Utilisation de partages fédérés
===============================

Le partage fédéré permet de monter des partages de serveurs ownCloud distants, en fait, 
en créant votre propre nuage de serveurs ownCloud. Vous pouvez créer des liens de partage directs
avec des utilisateurs d'autres serveurs ownCloud.

Création d'un nouveau partage fédéré
------------------------------------

Le partage fédéré est activé par défaut sur les nouvelles installations ou sur les installations
ownCloud mises à jour. Suivez ces étapes pour créer un nouveau partage avec d'autres serveurs ownCloud 9 :

1. Rendez-vous sur votre page ``Fichiers`` et cliquez sur l'icône de partage sur le fichier ou le dossier
que vous voulez partager. Dans la barre latérale, saisissez le nom d'utilisateur distant et l'URL du serveur ownCloud distant
sous cette forme : ``<nom_utilisateur>@<url_serveur_owncloud_distant>``. Dans cet exemple, c'est
``layla@serveur-distant/owncloud``. Le formulaire réplique automatiquement l'adresse que vous saisissez et la libelle 
« distant ». Cliquez sur le libellé.

.. figure:: ../images/direct-share-1.png

2. Quand votre serveur local ownCloud réussit à se connecter au serveur ownCloud
distant, un message de confirmation apparaît. Votre seule option de partage est **Peut 
modifier**. 
   
Cliquez sur le bouton Partager à tout moment pour voir avec qui vous avez partagé votre fichier. Supprimez
le partage à tout moment en cliquant sur l'icône représentant une corbeille. Ceci supprime seulement le partage
et ne supprime aucun fichier.

Création d'un nouveau partage fédéré par courriel
-------------------------------------------------

Utiliser cette méthode pour faire des partages avec des utilisateurs de ownCloud 8.x et précédents.

Que faire si vous ne connaissez pas le nom d'utilisateur ou l'URL ? ownCloud peut créer le lien pour vous
et l'envoyer à votre correspondant. 

.. figure:: ../images/create_public_share-6.png

Quand votre correspondant reçoit votre courriel, il devra accomplir quelques étapes 
pour accéder au partage. Il devra d'abord ouvrir le lien reçu dans un navigateur
et cliquer sur le bouton **Ajouter à votre ownCloud**.

.. figure:: ../images/create_public_share-8.png

Le bouton **Ajouter à votre ownCloud** se transforme en un champ de formulaire et votre correspondant 
devra saisir l'URL de son serveur ownCloud dans ce champ et appuyer sur la touche « Entrée » ou cliquer
sur la flèche.

.. figure:: ../images/create_public_share-9.png

Ensuite, une fenêtre de confirmation apparaîtra. Il suffira alors de cliquer sur le
bouton **Ajouter un partage distant** et c'est fini.

.. figure:: ../images/create_public_share-10.png

Supprimez le partage à tout moment en cliquant sur l'icône représentant une corbeille. Ceci supprime
seulement le partage et ne supprime aucun fichier.
