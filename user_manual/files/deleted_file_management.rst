==============================
Gestion des fichiers supprimés
==============================

Quand vous supprimez un fichier dans ownCloud, il n'est pas supprimé de façon permanente immédiatement. 
Il est en fait déplacé dans une corbeille. Il n'est pas supprimé définitivement tant que vous ne le
supprimez pas manuellement ou que l'application Files le supprime pour faire de la place pour les
nouveaux fichiers.

Pour retrouver les fichiers supprimés, cliquez sur le bouton **Fichiers supprimés** 
sur la page Fichiers dans l'interface Web d'ownCloud. Vous aurez alors l'option de les restaurer
ou de les supprimer définitivement.

Quotas
------

Les fichiers supprimés ne sont pas comptabilisés dans le décompte de votre quota. Seuls les fichiers
de l'utilisateur sont pris en compte, pas ceux partagés par d'autres utilisateurs.
(Consulter :doc:`quota` pour en apprendre plus sur les quotas).

Que se passe-t-il quand des fichiers sont supprimés ?
-----------------------------------------------------

La suppression de fichiers partagés devient un peu compliquée, comme l'illustre le
scénario ci-après :

1. L'utilisateur 1 partage un dossier « test » avec les utilisateurs 2 et 3.
2. L'utilisateur 2 supprime un fichier/dossier « sub » à l'intérieur du dossier « test ».
3. Le dossier « sub » sera déplacé dans la corbeille de l'utilisateur 1 (le propriétaire) et de l'utilisateur 2
   (le destinataire).
4. Mais l'utilisateur 3 n'aura pas de copie de « sub » dans sa corbeille.

Quand l'utilisateur 1 supprime « sub », il est déplacé dans sa corbeille. Il est supprimé du partage
pour les utilisateurs 2 et 3, mais n'est pas placé dans leurs corbeilles.

Quand vous partagez des fichiers, les autres utilisateurs peuvent les copier, les renommer, les déplacer et les partager 
avec d'autres utilisateurs, tout comme s'il s'agissait de leurs propres fichiers sur leur ordinateur. ownCloud n'a pas 
de pouvoirs magiques pour empêcher cela.

Comment l'application Deleted Files gère-t-elle l'espace de stockage ?
----------------------------------------------------------------------

Pour s'assurer que les utilisateurs ne dépasse pas leur quota de stockage, l'application Deleted Files 
alloue un maximum de 50% de l'espace libre disponible courant pour les fichiers supprimés.
Si les fichiers supprimés dépasse cette limite, ownCloud supprime les fichiers les plus 
anciens (c'est-à-dire les fichiers dont les horodatages de suppression sont les plus anciens) 
jusqu'à atteindre la limite de 50%.

ownCloud vérifie l'âge des fichiers supprimés chaque fois que de nouveaux fichiers sont ajoutés aux 
fichiers supprimés. Par défaut, les fichiers supprimés restent dans la corbeille pendant 180 jours. L'administrateur 
du serveur ownCloud peut ajuster cette valeur dans le fichier ``config.php`` 
en modifiant la valeur de la variable ``trashbin_retention_obligation``. Les fichiers plus anciens que la valeur de
``trashbin_retention_obligation`` seront supprimés définitivement. 
De plus, ownCloud calcule l'espace disponible maximal chaque fois qu'un nouveau fichier est ajouté. Si les fichiers
supprimés excèdent l'espace maximal autorisé, ownCloud supprimera les fichiers les plus anciens jusqu'à ce que la
limite soit de nouveau atteinte.
