=====================================
Téléversements de fichiers volumineux
=====================================

Lors du téléversement de fichier par l'intermédiaire du client Web, ownCloud est limité par la
configuration de PHP et d'Apache. Par défaut, PHP est configuré pour des téléversements de seulement
deux mégaoctets. Cette taille n'étant pas particulièrement adaptée, nous recommandons que l'administrateur
d'ownCloud augmente les valeurs des variables ownCloud à des tailles appropriées pour
les utilisateurs.

La modification de certaines variables d'ownCloud nécessite des autorisations d'administrateur. Si vous avez besoin
d'une taille limite supérieure à celle définie par défaut ou définie par votre administrateur :

* Contactez votre administrateur pour demander l'augmentation des valeurs de ces variables.

* Consulter la section du `Manuel administrateur d'ownCloud  <https://doc.owncloud.org/server/9.0/admin_manual/configuration_files/big_file_upload_configuration.html>`_
  qui décrit comment gérer les limites de taille de fichiers.
