===================
Application Galerie
===================

L'application Images a été réécrite et améliorée et s'appelle désormais Galerie. Elle gère
plus de formats d'images, le tri, le zoom et le défilement. Elle permet aussi des personnalisations
avancées à l'aide de simples fichiers texte.

Sur votre page Fichiers d'ownCloud, cliquez sur la petite icône représentant quatre carrés, dans le coin
supérieur droit de la fenêtre, sous votre nom d'utilisateur pour ouvrir votre galerie. L'application Galerie
trouve automatiquement toutes les images dans vos dossiers ownCloud et ajoute en surimpression sur leurs
imagettes le nom des dossiers. Cliquez sur le nom de dossier pour l'ouvrir. Dans le coin supérieur gauche de
la fenêtre, vous disposez de deux options de tri : alphabétique et chronologique.

.. figure:: ../images/gallery-1.png
   :alt: Gallery folder thumbnails.

Après être entré dans un dossier, cliquer sur une image pour l'ouvrir en mode diaporama. 
Les fonctionnalités suivantes sont disponibles : sur la partie supérieure, un bouton de téléchargement
au centre, des boutons suivant et précédent à gauche et à droite et un bouton de fermeture à droite.
Sur la partie inférieure, un bouton de diaporma automatique à droite.

.. figure:: ../images/gallery-2.png
   :alt: Imagettes de dossiers de la galerie.

Configuration personnalisée
---------------------------
   
Vous pouvez personnaliser un album de la galerie à l'aide d'un simple fichier texte nommé
**gallery.cnf**, qui contient des paramètres structurés utilisant le langage de balises
`Yaml <https://fr.wikipedia.org/wiki/YAML>`_. Vous pouvez avoir plusieurs fichiers
**gallery.cnf**. Vous en avez besoin d'un à la racine de votre dossier personnel ownCloud 
qui définit les fonctionnalités globales, et vous pouvez en avoir un pour chaque album
si vous voulez un comportement différent selon les albums.

Fonctionnalités
^^^^^^^^^^^^^^^

Les fonctionnalités générales suivantes sont actuellement mises en œuvre :

* gestion native de SVG ;
* accès aux partages externes.

Les fonctionnalités relatives aux albums suivantes sont actuellement mises en œuvre :

* ajout d'un lien à un fichier contenant une description ;
* saisie d'une déclaration de droit d'auteur simple dans le fichier de configuration ;
* ajout d'un lien à un fichier contenant une déclaration de droit d'auteur ;
* définition d'un type de tri et de son ordre ;
* définition de la couleur d'arrière-plan ;
* définition de l'héritage de la configuration pour les sous-albums.

Les fonctionnalités de diaporama suivantes sont actuellement mises en œuvre :

* Affichage d'un bouton permettant de choisir la couleur d'arrière-plan, blanche ou noire,
  à utiliser pour l'image en cours de visualisation (pour les images avec arrière-plan
  transparent).

Paramétrage
^^^^^^^^^^^

Le fichier de configuration doit être nommé **gallery.cnf**. Vous pouvez avoir des fichiers
**gallery.cnf** pour chaque album. Pour activer les fonctionnalités globales, placez-en un
dans le dossier racine, symbolisé dans l'interface Web par l'icône représentant une maison.
(c'est-à-dire le répertoire ``data/<utilisateur>/files/``. . Consulter :ref:`un exemple 
<supported_variables_label>` dans la section **Fonctionnalités globales**.

.. note:: Vous devez rafraîchir la page de votre navigateur pour observer
   vos modifications.

Format
^^^^^^

UTF-8, **sans BOM**. Un fichier créé à partir de l'interface Web d'ownCloud fonctionne.

Structure
^^^^^^^^^

Vous devez inclure un commentaire en fin de fichier, de sorte qu'un utilisateur qui tombe sur ce
fichier sache à quoi il sert. Les commentaires doivent commencer par un signe dièse « # ».

L'espacement se fait avec deux espaces. **N'utilisez pas de tabulations**.

Consultez la `documentation du format YAML
<http://symfony.com/doc/current/components/yaml/yaml_format.html>`_ si vous obtenez des messages
d'erreur.

Voici un exemple de fichier `gallery.cnf`::

  # Fichier de configuration de Galerie
  # Créé le 31 Jan 2016 par l'utilisateur d'ownCloud Toto
 features:
   external_shares: yes
   native_svg: yes
   background_colour_toggle: yes
 design:
   background: "#ff9f00"
   inherit: yes
 information:
   description: Ceci est la **description d'un album** qui n'est affichée que s'il n'existe pas
   de `description_link`
   description_link: readme.md
   copyright: Copyright 2003-2016 [interfaSys sàrl](http://www.interfasys.ch), 
   Switzerland
   copyright_link: copyright.md
   inherit: yes
 sorting:
   type: date
   order: des
   inherit: yes
   
.. _supported_variables_label:   

Variables gérées
^^^^^^^^^^^^^^^^

**Fonctionnalités globales**

Placez ceci dans votre dossier racine d'ownCloud.

* **external_shares** : définir à **yes** dans votre fichier de configuration racine si vous 
  voulez charger les images stockées sur des emplacements externes en utilisant l'application
  **files_external**.
* **native_svg** : définir à **yes** dans votre fichier de configuration racine pour activer
  le rendu des images SVG dans votre navigateur. Cela peut représenter un risque de sécurité si vous ne
  pouvez pas avoir totalement confiance en vos fichiers SVG.
* **background_colour_toggle** : définir à **yes** dans votre fichier de configuration racine pour activer 
  un bouton qui permute la couleur de l'arrière-plan en noir ou en blanc pour les images à fond
  transparent.

.. note:: Les partages externes sont 20 à 30 fois plus lents que les partages locaux. Attendez-vous à
   patienter un long moment pour voir toutes les images contenues dans un album partagé.

**Configuration par album**

Chaque album peut être configuré individuellement en utilisant les sections de configuration suivantes.
Utiliser le paramètre **inherit** pour propager la configuration aux sous-albums.

**Apparence**

* **background** : définit la couleur de l'arrière-plan du mur de photos
  en utilisant la représentation RGB hexadécimale de cette couleur. Par exemple : 
  **"#ffa033"**. Vous devez utiliser des guillemets doubles (anglais) autout de la valeur
  sinon celle-ci sera ignorée. Il est fortement recommandé d'utiliser un thème personnalisé, avec un sélecteur
  de couleurs CSS si vous comptez utiliser cette fonctionnalité. Vous pouvez utiliser `cette palette 
  de couleurs <http://paletton.com/>`_ pour trouver une couleur qui vous plaît.
* **inherit** : définir à **yes** pour que les sous-albums héritent de cette partie de la configuration.

**Présentation d'album**

* **description** : une chaîne formatée en Markdown qui sera affichée dans la boîte
  d'information. Elle peut s'étendre sur plusieurs lignes en utilisant les balises Yaml.
* **description_link** : un fichier en Markdown situé dans le dossier de l'album qui sera analysé
  et affiché dans la boîte d'information à la place de la description.
* **copyright** : une chaîne formatée en Markdown. Des liens vers des ressources externes peuvent y
  être ajoutés.
* **copyright_link** : tout fichier (par ex. copyright.html), dans l'album, 
  qui sera téléchargé lorsque l'utilisateur clique sur le lien.
* **inherit** : définir à **yes** pour que les sous-albums héritent de cette partie de la configuration.

Consulter `<http://www.markitdown.net/markdown>`_ pour la syntaxe Markdown.

.. note:: N'ajoutez pas de lien dans votre chaîne `copyright` si vous utilisez la
   variable **copyright_link**.

**Tri**

* **sorting** : **date** ou **name**. **date** ne fonctionne que pour les fichiers.
* **sort_order** : **asc** ou **des** (croissant ou décroissant).
* **inherit** : définir à **yes** pour que les sous-albums héritent de cette partie de la configuration.

Notes
-----

* Quand seule la variable **type** a été définie, l'ordre de tri par défaut
  sera utilisé.
* Quand seule la variable de tri **order** a été trouvée, la configuration  de tri
  sera ignorée et le script continuera de chercher une configuration valide dans 
  les dossiers supérieurs.
* Pour activer une fonctionnalité comme le rendu natif des images SVG dans un dossier public,
  vous devez créer dans ce dossier un fichier de configuration contenant cette fonctionnalité.
* Si vous partagez un dossier publiquement, n'oubliez pas d'ajouter tous les fichiers que vous liez
  (par ex. : ``description.md`` ou ``copyright.md``) dans le dossier partagé car l'utilisateur n'aura
  pas accès aux fichiers dans le dossier parent.
* Puisque les utilisateurs peuvent télécharger tout un dossier sous la forme d'une archive, il est conseillé 
  d'inclure tous les fichiers dans le dossier partagé plutôt que d'ajouter le texte dans le fichier
  de configuration.

Exemples
--------

**Tri seulement**

S'applique au dossier courant seulement::

 # Fichier de configuration de Galerie
   sorting:
   type: date
   order: asc

Courte description et lien vers le document de copyright, s'appliquant au dossier courant
et à ses sous-dossiers. Cet exemple montre également comment utiliser la syntaxe pour écrire la
description sur plusieurs lignes::

 # Fichier de configuration de Galerie
   information:
   description: | # La Maison Bleue, hiver 2016
     Il s'agit de notre collection hiver 2016 tournée à **Kyoto**
     Visiter notre [site Web](http://www.secretdesigner.ninja) pour plus d'informations
   copyright: Copyright 2015 La Maison Bleue, France
   copyright_link: copyright_2015_lmb.html
   inherit: yes

**Chargement d'images à partir de stockages externes**

.. note:: Les fonctionnalités ne peuvent être définies que dans le dossier racine.

Vous pouvez ajouter des éléments de configuration standards dans le même fichier de configuration::

 # Fichier de configuration de Galerie
   features:
   external_shares: yes

**Activation du rendu SVG natif**

.. note:: Les fonctionnalités spéciales ne peuvent être définies que dans le dossier racine.

Vous pouvez ajouter des éléments de configuration standards dans le même fichier de configuration::

 # Fichier de configuration de Galerie
  features:
  native_svg: yes
  
Futures extensions possibles
----------------------------

Différents paramètres de tris pour les albums.

Suivre les nouveautés
---------------------

Consulter la `page wiki de Gallery <https://github.com/owncloud/gallery/wiki>`_ pour rester informé des derniers développements.
