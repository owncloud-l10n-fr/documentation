=================
Quota de stockage
=================

Votre administrateur ownCloud a la possibilité de définir des quotas pour les utilisateurs.
Regardez en haut de votre page personnelle pour connaître ce quota et l'espace que
vous utilisez.

.. figure:: ../images/quota1.png

Il peut être utile de comprendre comment ce quota est calculé.

Les métadonnées (imagettes, fichiers temporaires, cache et clés de chiffrement) prennent
jusqu'à 10% de l'espace disque, mais celles-ci ne sont pas prise en compte dans le quota.
Certaines applications stockent des informations dans la base de données, comme les 
applications Agenda et Contacts. Ces données ne sont pas non plus prises en compte dans le
calcul du quota.

Quand d'autres utilisateurs partagent des fichiers avec vous, les fichiers partagés ne sont pris
en compte que dans le quota de leurs propriétaires. Quand vous partagez un dossier et permettez à
d'autres utilisateurs de téléverser des fichiers dans ce partage, tous les fichiers téléversés et
modifiés seront pris en compte dans le calcul de votre quota. Quand vous re-partagez des fichiers,
ceux-ci ne sont pas pris en compte dans votre quota mais dans celui de leur propriétaire.

Les fichiers chiffrés sont un peu plus gros que les fichiers non chiffrés. La taille du fichier non
chiffré est utilisée pour le calcul de votre quota.

Les fichiers supprimés qui sont encore dans la corbeille ne sont pas pris en compte dans le calcul
des quotas. La corbeille est définie à 50% du quota. La suppression des fichiers de la corbeille
est définie à 30 jours. Quand la taille des fichiers présents dans la corbeille excède 50% du quota, 
les fichiers les plus anciens sont supprimés jusqu'à atteindre moins de 50%.

Quand le contrôle de version est activé, les anciennes versions des fichiers ne sont pas prises
en compte dans le calcul du quota.

Si vous créez un partage par URL et que vous autorisez les téléversements, les fichiers téléversés
seront pris en compte dans le calcul du quota.
