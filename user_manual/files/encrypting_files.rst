======================================
Chiffrement des fichiers dans ownCloud
======================================

ownCloud contient une application Chiffrement qui, lorsqu'elle est activée par
votre administrateur d'ownCloud, chiffre automatiquement tous les fichiers de
données d'ownCloud. Le chiffrement s'applique à tout le serveur. Par conséquent,
quand elle est activée, vous ne pouvez pas choisir de ne pas chiffrer vos fichiers.
Vous n'avez rien de particulier à faire car elle utilise votre nom de connexion
ownCloud et votre mot de passe pour votre unique clé privée de chiffrement. Il suffit
juste de se connecter et se déconnecter et partager vos fichiers normalement. Vous
pouvez toujours changer votre mot de passe.

Son but principal est de chiffrer les fichiers sur les services de stockage distants 
connectés à votre serveur ownCloud, tels que Dropbox et Google Drive. C'est un
moyen simple et facile pour protéger vos fichiers sur un stockage distant. Vous
pouvez partager vos fichiers distants avec ownCloud de manière habituelle,
cependant, vous ne pouvez pas partager vos fichiers chiffrés directement depuis
Dropbox, Google Drive ou quelque service que vous utilisiez, car les clés de chiffrement
sont stockées sur votre serveur ownCloud et ne sont jamais exposées aux fournisseurs de
services externes.

Si votre serveur ownCloud n'est connecté à aucun service de stockage distant, il est alors
préférable d'utiliser une autre forme de chiffrement, comme le chiffrement au niveau fichier
ou au niveau disque. Parce que les clés sont conservées sur votre serveur ownCloud, il est
possible pour votre administrateur de jeter un œil sur vos fichiers, et si le serveur est
compromis, l'intrus peut accéder à vos fichiers. (Lire 
`Comment ownCloud utilise le chiffrement pour protéger vos données
<https://owncloud.org/blog/how-owncloud-uses-encryption-to-protect-your-data/>`_ 
pour en apprendre plus).

Utilisation du chiffrement
--------------------------

Le chiffrement d'ownCloud se présente à peu près ainsi : paramétrez-le et oubliez-le. Il
dispose cependant de quelques options que vous pouvez utiliser.

Quand l'administrateur de votre serveur ownCloud active le chiffrement pour la première fois,
vous devez vous déconnecter puis vous reconnecter pour créer vos clés de chiffrement et chiffrer
vos fichiers. Quand le chiffrement a été activé sur votre serveur ownCloud, vous verrez une
bannière jaune sur votre page Fichiers et un avertissement vous invitant à vous déconnecter et
à vous reconnecter.

.. figure:: ../images/encryption1.png

Quand vous vous reconnectez, cela prend quelques minutes pour fonctionner, en fonction du nombre de
fichiers que vous avez, puis, vous êtes renvoyé sur la page par défaut d'ownCloud.

.. figure:: ../images/encryption2.png


.. note:: Ne perdez pas votre mot de passe ownCloud car vous perdriez vos fichiers.
   Il existe cependant une option de récupération que votre administrateur peut activer.
   Consulter la section Récupération de mot de passe (ci-dessous) pour en apprendre plus à ce sujet.
   
Partage des fichiers chiffrés
------------------------------

Seuls les utilisateurs ayant des clés de chiffrement privées peuvent accéder aux
partages de fichiers et de dossiers chiffrés. Les utilisateurs n'ayant pas encore créé
leurs clés de chiffrement privées ne pourront pas accéder aux partages chiffrés.
Ils pourront voir les noms des fichiers et dossiers mais ne pourront ni ouvrir, ni
télécharger de fichiers. Ils verront une bannière d'avertissement jaune indiquant :
« L'application de chiffrement est activée mais vos clefs ne sont pas initialisées.
Veuillez vous déconnecter et ensuite vous reconnecter. ».

Les propriétaires de partages peuvent avoir à re-partager leurs fichiers après l'activation
du chiffrement. Les utilisateurs essayant d'accéder au partage verront un message les invitant
à demander au propriétaire du partage à re-partager les fichiers avec eux.
Pour les partages indivisuels, stopper le partage et réactivez-le.
Pour les partages de groupes, partagez avec chaque utilisateur qui ne peuvent accéder
au partage. Ceci met à jour le chiffrement et le propriétaire du partage peut supprimer
les partages individuels.

Clé de récupération
~~~~~~~~~~~~~~~~~~~

Si votre administrateur ownCloud a activé la fonctionnalité de clé de récupération, vous pouvez 
choisir d'utiliser cette fonctionnalité pour votre compte. Si vous activez « Récupération de mot de passe », 
l'administrateur peut lire vos données avec un mot de passe spécial. Cette fonctionnalité permet à l'administrateur
de récupérer vos fichiers dans le cas où vous auriez perdu votre mot de passe owncloud.
Si la clé de récupération n'est pas activée, il n'y a alors aucun moyen de restaurer vos fichiers
si vous perdez votre mot de passe de connexion.

.. figure:: ../images/encryption3.png

Fichiers non chiffrés
---------------------

Seules les données dans vos fichiers sont chiffrées, pas les noms de fichiers ou de 
dossiers. Ces fichiers ne sont jamais chiffrés :

- les anciens fichiers dans la corbeille ;
- les aperçus d'images de l'application Galerie ;
- les aperçus de l'application Fichiers ;
- l'index de recherche de l'application de recherche de texte ;
- les données des applications tierces.

Il peut y avoir d'autres fichiers qui ne sont pas chiffrés. Seuls les fichiers exposés
aux fournisseurs de stockage tiers sont assurés d'être chiffrés.

Changement du mot de passe de la clé privée
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cette option n'est disponible que si votre mot de passe de connexion, mais pas votre
mot de passe de chiffrement, a été changé par l'administrateur. Ceci peut survenir si
ownCloud utilise un système d'authentification externe (par exemple, LDAP) et que votre mot de
passe de connexion a été changé en utilisant ce système. Dans ce cas, vous pouvez changer
votre mot de passe de chiffrement pour le nouveau mot de passe en fournissant votre ancien et votre
nouveau mot de passe de connexion. L'application Chiffrement ne fonctionne que si votre mot de
passe de conexion et votre mot de passe de chiffrement sont identiques.
