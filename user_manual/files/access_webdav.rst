===============================================
Accès aux fichiers ownCloud en utilisant WebDAV
===============================================

ownCloud gère totalement le protocole WebDAV et vous pouvez vous connecter et synchroniser 
vos fichiers avec ownCloud en WebDAV. Dans ce chapitre, vous apprendrez comment vous
connecter sous GNU/Linux, Mac OS X, Windows et les appareils mobiles à votre serveur ownCloud en
WebDAV. Avant d'aborder la configuration de WebDAV, regardons rapidement la manière recommandée
pour connecter les clients aux serveurs ownCloud.

Clients ownCloud pour ordinateurs et mobiles
--------------------------------------------

La méthode recommandée pour garder votre ordinateur synchronisé avec votre serveur
ownCloud est d'utiliser le `client ownCloud pour ordinateur
<https://owncloud.org/install/#install-clients>`_. Vous pouvez configurer le client ownCloud
pour enregistrer les fichiers dans tout répertoire local que vous souhaitez, et vous pouvez choisir
quels répertoires du serveur ownCloud synchroniser. Le client affiche l'état de connexion en
cours et tient un journal de toute l'activité de sorte que vous sachiez toujours quels fichiers ont été
téléchargés sur votre ordinateur. Vous pouvez vérifier que les fichiers qui ont été créés et mis à jour 
sur votre ordinateur local sont correctement synchronisés avec le serveur.

La méthode recommandée pour la synchronisation des appareils fonctionnant avec Android et
Apple iOS est d'utiliser les `applications ownCloud pour mobiles
<https://owncloud.org/install/#install-clients>`_.

Pour vous connecter à votre serveur ownCloud avec les applications pour mobiles **ownCloud**, utiliser l'URL et
le dossier de base seulement::

    exemple.com/owncloud

En plus des applications pour mobiles fournies par ownCloud, vous pouvez utiliser d'autres applications pour vous
connecter à votre serveur ownCloud avec votre équipement mobile en utilisant WebDAV. `WebDAV Navigator`_ est une 
bonne application (propriétaire) pour les `appareils Android`_, `les iPhones`_ et `les BlackBerry`_.
L'URL à utiliser sur ceux-ci est la suivante::

    exemple.com/owncloud/remote.php/dav
    
Configuration WebDAV
--------------------

Si vous préférez, vous pouvez aussi connecter votre ordinateur à votre serveur ownCloud en utilisant
le protocole WebDAV plutôt que d'utiliser une application cliente spéciale. Web 
Distributed Authoring and Versioning (WebDAV) est une extension du protocole de transfert hypertexte 
(HTTP) qui facilité la création, la lecture et la modification des fichiers sur les serveurs
Web. Avec WebDAV, vous pouvez accéder à vos partages ownCloud sous GNU/Linux, Mac OS X et 
Windows de la même manière que n'importe quel autre partage réseau, et rester synchonisé.

.. note:: Dans les exemples suivants, vous devez remplacer **exemple.com/** par
   l'URL de votre serveur ownCloud.

Accès aux fichiers en utilisant GNU/Linux
-----------------------------------------

Vous pouvez accéder aux fichiers sur les systèmes d'exploitation GNU/Linux en utilisant les méthodes suivantes.

Gestionnaire de fichiers Nautilus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Utiliser le protocole ``davs://`` pour connecter le gestionnaire de fichiers Nautilus à votre partage
ownCloud::

  davs://exemple.com/owncloud/remote.php/dav

.. note:: Si votre connexion serveur n'est pas sécurisée en HTTPS, utilisez `dav://` au 
   lieu de `davs://`.

.. image:: ../images/webdav_gnome3_nautilus.png
   :alt: Copie d'écran du gestionnaire de fichiers Nautilus pour utiliser WebDAV

Accès aux fichiers avec KDE et le gestionnaire de fichiers Dolphin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour accéder à vos fichiers ownCloud en utilisant les gestionnaire de fichiers Dolphin dans un bureau KDE, utilisez 
le protocole ``webdav://``::

    webdav://exemple.com/owncloud/remote.php/dav

Vous pouvez créer un lien permanent vers votre serveur ownCloud :

#. Ouvrez Dolphin et cliquez sur « Réseau » dans la colonne de gauche « Emplacements ».
#. Cliquez sur l'icône libellée **Ajouter un dossier réseau**.
   Le dialogue résultant devrait apparaître avec WebDAV déjà sélectionné.
#. Si ce n'est pas le cas, sélectionnez-le.
#. Cliquez sur **Suivant**.
#. Saisissez les paramètres suivants :

   * Nom : le nom que vous voulez voir apparaître dans le signet **Emplacements**, par exemple ownCloud ;

   * Utilisateur : le nom d'utilisateur ownCloud que vous utilisez pour vous connecter, par exemple admin ;
   
   * Serveur : le nom de domain ownCloud, par exemple **exemple.com** (sans 
     **http://** devant ou un nom de répertoire après) ;
   * Dossier : saississez le chemin ``owncloud/remote.php/dav``.
#. (Optionnel) : cochez la case « Créer une icône » pour faire apparaître un marque-page dans la
   colonne « Emplacements ».
#. (Optionnel) : fournissez des paramètres spéciaux ou un certficat SSL dans la case « Port & 
   chiffrement ».

.. image:: ../images/webdav_dolphin1.png
   :alt: Copie d'écran de la configuration du gestionnaire de fichiers Dolphin pour utiliser WebDAV.
  
.. image:: ../images/webdav_dolphin2.png
   :alt:

.. image:: ../images/webdav_dolphin3.png

Création de points de montage WebDAV en ligne de commande sous GNU/Linux
------------------------------------------------------------------------

Vous pouvez créer des points de montage WebDAV en ligne de commande sous GNU/Linux. Ceci est utile si vous
préférez accéder à ownCloud de la même manière que les autres points de montage.
L'exemple suivant montre comment créer un point de montage personnel et le monter automatiquement
chaque fois que vous vous connectez sur votre ordinateur.

1. Installez le pilote de système de fichiers ``davfs2``, qui permet de monter des partages
   WebDAV comme n'importe quel autre système de fichiers distant. Utilisez la commande suivante pour
   l'installer sous Debian/Ubuntu::
   
    apt-get install davfs2
    
2. Utilisez cette commande sous CentOS, Fedora et openSUSE::

    yum install davfs2    

3. Ajoutez-vous au groupe ``davfs2``::

    usermod -aG davfs2 <uitilisateur>

3. Créez ensuite un répertoire ``owncloud`` dans votre répertoire utilisateur (/home/<utilisateur>) pour le point de 
   montage et le répertoire ``.davfs2/`` pour votre fichier de configuration personnel::
   
    mkdir ~/owncloud
    mkdir ~/.davfs2
    
4. Copiez ``/etc/davfs2/secrets`` dans ``~/.davfs2``:: 

    cp  /etc/davfs2/secrets ~/.davfs2/secrets 
   
5. Rendez-vous propriétaire du fichiers et ajoutez les permission de lecture-écriture pour le propriétaire seulement::
    
    chown <utilisateur>:<utilisateur>  ~/.davfs2/secrets
    chmod 600 ~/.davfs2/secrets
 
6. Ajoutez vos identifiants de connexion ownCloud à la fin du fichier ``secrets``, 
   en utilisant l'URL de votre serveur ownCloud et vos nom d'utilisateur et mot de passe ownCloud::

    exemple.com/owncloud/remote.php/dav <utilisateur> <mot-de-passe>

7. Ajoutez les informations de montage dans le fichier ``/etc/fstab``::

    exemple.com/owncloud/remote.php/dav /home/<utilisateur>/owncloud 
    davfs user,rw,auto 0 0

8. Ensuite, testez que le montage et l'authentification fonctionnent en exécutant la commande suivante.
   Si vous l'avez paramétré correctement, vous n'aurez pas besoin de permissions root::

    mount ~/owncloud
    
9. Vous devriez aussi pouvoir le démonter::
 
    umount ~/owncloud
    
Désormais, chaque fois que vous vous connecterez sur votre système GNU/Linux, votre partage ownCloud
devrait automatiquement se monter en utilisant WebDAV dans votre répertoire``~/owncloud``. Si vous préférez
le monter manuellement, changez ``auto`` pour ``noauto`` dans le fichier ``/etc/fstab``.

Problèmes connus
----------------

Problème
^^^^^^^^
Ressource temporairement indisponible

Solution
^^^^^^^^
Si vous rencontrez des problèmes lors de la création de fichiers dans le répertoire,
modifiez le fichier ``/etc/davfs2/davfs2.conf`` et ajoutez-y::

    use_locks 0

Problème
^^^^^^^^
Avertissements de certificat

Solution
^^^^^^^^ 

Si vous utilisez un certificat auto-signé, vous obtiendrez des avertissements. Pour changer cela, 
vous devez configurer ``davfs2`` pour qu'il reconnaisse votre certificat. 
Copiez votre certificat ``moncertificat.pem`` dans ``/etc/davfs2/certs/``. Ensuite, éditez le fichier
``/etc/davfs2/davfs2.conf`` et enlevez le commentaire pour la ligne ``servercert``. Puis, ajoutez le chemin
d'accès à votre certifcat comme dans l'exemple ci-dessous::

 servercert   /etc/davfs2/certs/moncertificat.pem

Accès aux fichiers en utilisant Mac OS X
----------------------------------------

.. note:: Le Finder Mac OS X souffre d'une `série de problèmes de mise en œuvre 
   <http://sabre.io/dav/clients/finder/>`_ et ne devrait être utilisé que si votre serveur
   ownCloud fonctionne avec **Apache** et **mod_php** ou **Nginx 1.3.8+**.

Pour accéder aux fichiers avec le Finder de Mac OS X Finder :

1. Choisissez **Aller > Se connecter au serveur**.

  La fenêtre  « Connexion au serveur » s'ouvre.

2. Indiquez l'adresse du serveur dans le champ **Adresse du serveur**.

  .. image:: ../images/osx_webdav1.png
     :alt: Copie d'écran de la saisie de l'adresse du serveur ownCloud dans Mac OS X

  Par exemple, l'URL utilisée pour se connecter au serveur ownCloud
  dans le Finder de Mac OS X Finder est::

    https://exemple.com/owncloud/remote.php/dav

  .. image:: ../images/osx_webdav2.png

3. Cliquez sur **Se connecter**.

   L'appareil se connecte au serveur.

Pour plus de détails sur les façons de se connecter à un serveur distant en utilisant Mac OS X, 
consulter la `documentation du vendeur 
<https://support.apple.com/kb/PH21629?viewlocale=fr_FR&locale=fr_FR>`_

Accès aux fichiers en utilisant Microsoft Windows
-------------------------------------------------

Il est préférable d'utiliser un client WebDAV approprié de la
`page du projet WebDAV <http://www.webdav.org/projects/>`_ .

Si vous devez utiliser le client WebDAV natif de Windows, vous pouvez connecter un lecteur
sur votre partage ownCloud. Ceci permet de parcourir les fichiers sur un serveur ownCloud 
de la même manière que n'importe quel autre lecteur réseau.

Cette fonctionnalité nécessite la connectivité au réseau. Si vous voulez stocker vos fichiers en mode
hors ligne, utilisez le client pour ordinateur ownCloud pour synchroniser tous les fichiers de votre compte
ownCloud dans un ou plusieurs répertoires de votre disque local.

.. note:: Avant de connecter votre lecteur à ownCloud, vous devez permettre l'utilisation de
  Authentification basique dans la base de registre de Windows. Cette procédure est documentée dans
  KB841215_ et diffère selon les versions Windows XP/Server 2003 et Windows Vista/7.
  Veuillez suivre l'article de la base de connaissance avant de commencer et suivez les instructions pour
  Vista si vous utilisez Windows 7.

.. _KB841215: http://support.microsoft.com/kb/841215

Connexion d'un lecteur en ligne de commande
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'exemple suivant montre comment connecter un lecteur réseau en utilisant la ligne de
commande :

1. Ouvrez une invite de commande dans Windows.
2. Saisissez la ligne suivante dans l'invite de commande pour connecter le lecteur Z 
   à ownCloud::

    net use Z: https://<chemin_d_acces>/remote.php/dav /user:utilisateur 
    motdepasse

  où <chemin_d_acces> est l'URL de votre serveur ownCloud.

Par exemple : ``net use Z: https://exemple.com/owncloud/remote.php/dav 
/user:utilisateur motdepasse``

  L'ordinateur associera les fichiers de votre compte ownCloud à la lettre de lecteur Z.

.. note:: Bien que ce ne soit pas recommandé, vous pouvez aussi monter le serveur ownCloud
     en utilisant HTTP, laissant ainsi la connexion non chiffrée. Si vous projetez d'utiliser des connexions HTTP
     sur vos équipements dans un lieu public, nous recommandons vivement d'utiliser un tunnel
     VPN pour avoir la sécurité nécessaire.

Une syntaxe alternative de ligne commande::

  net use Z: \\exemple.com@ssl\owncloud\remote.php\dav /user:utilisateur 
  motdepasse

Connexion de lecteurs avec l'explorateur de fichiers de Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour connecter un lecteur en utilisant l'explorateur de fichiers de Microsoft Windows :

1. Ouvrez le gestionnaire de fichiers.
2. Faites un clic droit sur **Ordinateur** et sélectionnez **Connecter un lecteur réseau...** dans le 
   menu déroulant.
3. Choisissez un lecteur local auquel associer vos fichiers ownCloud.
4. indiquez l'adresse de votre serveur ownCloud suivie de
   **/remote.php/dav**.

  Par exemple::

    https://exemple.com/owncloud/remote.php/dav

.. note:: Pour les serveurs sécurisés en SSL, cochez la case **Se reconnecter à l'ouverture de session** pour que le
     lecteur soit reconnecté à chaque redémarrage. Si vous souhaitez vous connecter au serveur ownCloud en utilisant
     un nom d'utilisateur différent, cochez la case **Se connecter à l'aide d'informations d'identification différentes**.

.. figure:: ../images/explorer_webdav.png
   :scale: 80%
   :alt: Copie d'écran d'une connexion WebDAV dans le gestionnaire de fichiers de Windows

5. Cliquez sur le bouton ``Terminer``.

  Le gestionnaire de fichiers de Windows connecte alors le lecteur réseau rendant les fichiers de votre
  compte ownCloud accessibles.

Accéder aux fichiers en utilisant Cyberduck
-------------------------------------------

`Cyberduck <https://cyberduck.io/?l=en>`_ est un navigateur libre FTP et SFTP, 
WebDAV, OpenStack Swift et Amazon S3 conçu pour le transfert de fichiers sous
Mac OS X et Windows.

.. note:: Cet exemple utilise Cyberduck version 4.2.1.

Pour utiliser TCyberduck :

1. Indiquer un serveur sans spécifier d'informations de protocole. Par exemple :

  ``exemple.com``

2. Spécifiez le port approprié. Le port que vous choisissez dépend de l'utilisation ou non
par votre serveur ownCloud de SSL. Cyberduck nécessite de sélectionner un type de connexion
différent si vous projetez d'utiliser SSL. Par exemple :

  80 (pour WebDAV)
  
  443 (pour WebDAV (HTTPS/SSL))

3. Utilisez le menu déroulant « Plus d'options » pour ajouter le reste de votre URL WebDAV 
dans le champ « Chemin ». Par exemple :

  ``remote.php/dav``

Cyberduck active alors l'accès aux fichiers de votre serveur ownCloud.

Accès aux partages publics en utilisant WebDAV
----------------------------------------------

ownCloud offre la possibilité de se connecter aux partages publics en utilisant WebDAV.

Pour accéder au partage public, ouvrez l'adresse::

  https://example.com/owncloud/public.php/dav

dans un client WebDAV, utilisez le jeton de partage comme nom d'utilisateur et éventuellement le mot de passe de partage le cas
échéant.

Problèmes connus
----------------

Problème
^^^^^^^^
Windows ne se connecte pas en utilisant HTTPS.

Solution 1
^^^^^^^^^^

Le client WebDAV de Windows ne gère pas l'indication de nom de serveur (Server Name Indication (SNI)) sur les 
connexions chiffrées. Si vous rencontrez une erreur lors du montage d'une instance ownCloud chiffrée par SSL,
contactez votre fournisseur pour avoir une adresse IP dédiée pour votre serveur sécurisé par SSL.

Solution 2
^^^^^^^^^^

Le client WebDAV de Windows pourrait ne pas gérer les connexionsTSLv1.1 et TSLv1.2. Si vous avez
restreint la configuration de votre serveur pour n'autoriser que TLSv1.1 et supérieurs, la connexion
à votre serveur pourrait échouer. Veuillez consulter la documentation WinHTTP_
pour de plus amples informations.

.. _WinHTTP: https://msdn.microsoft.com/en-us/library/windows/desktop/aa382925.aspx#WinHTTP_5.1_Features

Problème
^^^^^^^^

Vous recevez le message d'erreur suivant : **Error 0x800700DF: The file size 
exceeds the limit allowed and cannot be saved** (La taille du fichier dépasse la limite autorisée
et il ne peut être enregistré).

Solution
^^^^^^^^

Windows limite la taille maximale d'un fichier transféré sur un partage WebDAV.
Vous pouvez augmenter la valeur **FileSizeLimitInBytes** dans la base de registre de Windows
en modifiant la clé : 
**HKEY_LOCAL_MacHINE\\SYSTEM\\CurrentControlSet\\Services\\WebClient\\Parameters
** en sélectionnant **Modifier...**.

Pour augementer la valeur à un maximum de 4 Go, sélectionnez **Décimale**, saisissez la 
valeur **4294967295**, cliquez sur le bouton OK puis redémarrez Windows ou redémarrez le
service **WebClient**.

Problème
^^^^^^^^

L'accès à vos fichiers dans Microsoft Office sur un partage WebDAV échoue.

Solution
^^^^^^^^

Les problèmes connus et leurs solutions sont détaillés dans l'article KB2123563_ .

Problème
^^^^^^^^

Impossible de se connecter avec WebDAV à ownCloud sous Windows en utilisant un certificat auto-signé.

Solution
^^^^^^^^

  #. Rendez-vous sur votre instance ownCloud à l'aide d'Internet Explorer.
  #. Cliquez jusqu'à obtenir l'erreur de certificat dans la barre d'état du navigateur.
  #. Afficher le certificat, puis dans l'onglet Détails, enregistrez-le sur votre ordinateur en cliquant sur « Exporter... ».
  #. Enregistrez-le sous un nom quelconque, par exemple ``monOwnCloud.cer``.
  #. Cliquez sur le menu Démarrer, dans le champ « Rechercher des programmes et fichiers » saisissez « MMC » puis appuyez sur
     sur la touche « Entrée ».
  #. Sélectionnez le menu « Fichier > Ajouter/Supprimer un composant logiciel enfichable... ».
  #. Sélectionnez « Certificats », cliquez sur « Ajouter > », choisissez « Mon compte utilisateur », puis cliquez sur « Terminer »
     puis « OK ».
  #. Développez l'arborescence « Certificats - Utilisateur actuel » et sélectionnez « Autorités de certification racines
     de confiance > Certificats ».
  #. Faites un clic droit sur « Certificats » et sélectionnez « Toutes les tâches > Importer... ».
  #. Sélectionnez le certificat enregistré sur votre ordinateur puis cliquez sur le bouton « Suivant ».
  #. Sélectionnez  « Placer tous les certificats dans le magasin suivant » et cliquez sur le bouton « Parcourir... ».
  #. Cochez la case « Afficher les magasins physiques », développez la branche « Autorités de certification racines
     de confiance » et sélectionnez  « Ordinateur local ». Cliquez sur « OK », puis « Suivant » et « Terminer ».
  #. Vérifiez dans la liste que le certificat est présent. Vous devrez peut-être actualiser l'affichage avant de le
     voir. quittez la console MMC.
  #. Ouvrez le navigateur, sélectionnez « Outils » puis « Supprimer l'historique de navigation ».
  #. Sélectionnez tout sauf « Données de protection contre le tracking, ... » puis cliquez sur « Supprimer ».
  #. Rendez-vous dans le menu « Options Internet » (icône représentant un engrenage en haut à droite). Sélectionnez l'onglet
     « Contenu » puis cliquez sur le bouton « Effecer l'état SSL ».
  #. Fermez le navigateur, puis relancez-le et testez.
  
Problème
^^^^^^^^

Vous ne pouvez pas télécharger ou téléverser des fichiers de 50 Mo ou plus quand cela prend plus de 30 minutes
en utilisant le client Web dans Windows 7.

Solution
^^^^^^^^

Les moyens de contournement sont documentés ans l'article KB2668751_.


Accès aux fichiers en utilisant cURL
------------------------------------

Puisque WebDAV est une extension de HTTP, cURL peut être utilisé pour scripter les opérations sur les fichiers.
	
Pour créer un dossier avec la date du jour comme nom :

.. code-block:: bash

	$ curl -u utilisateur:mot-de-passe -X MKCOL "https://exemple.com/owncloud/remote.php/dav/$(date '+%d-%b-%Y')"

Pour téléverser un fichier ``erreur.log`` dans ce répertoire :

.. code-block:: bash

	$ curl -u utilisateur:mot-de-passe -T erreur.log "https://exemple.com/owncloud/remote.php/dav/$(date '+%d-%b-%Y')/erreur.log"

Pour déplacer un fichier :

.. code-block:: bash

	$ curl -u utilisateur:mot-de-passe -X MOVE --header 'Destination: https://exemple.com/owncloud/remote.php/dav/target.jpg' https://exemple.com/owncloud/remote.php/dav/source.jpg

Pour obtenir les propriétés des fichiers dans le dossier racine :

.. code-block:: bash

	$ curl -X PROPFIND -H "Depth: 1" -u utilisateur:mot-de-passe https://exemple.com/owncloud/remote.php/dav/ | xml_pp
	<?xml version="1.0" encoding="utf-8"?>
    <d:multistatus xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:s="http://sabredav.org/ns">
      <d:response>
        <d:href>/owncloud/remote.php/dav/</d:href>
        <d:propstat>
          <d:prop>
            <d:getlastmodified>Tue, 13 Oct 2015 17:07:45 GMT</d:getlastmodified>
            <d:resourcetype>
              <d:collection/>
            </d:resourcetype>
            <d:quota-used-bytes>163</d:quota-used-bytes>
            <d:quota-available-bytes>11802275840</d:quota-available-bytes>
            <d:getetag>"561d3a6139d05"</d:getetag>
          </d:prop>
          <d:status>HTTP/1.1 200 OK</d:status>
        </d:propstat>
      </d:response>
      <d:response>
        <d:href>/owncloud/remote.php/dav/welcome.txt</d:href>
        <d:propstat>
          <d:prop>
            <d:getlastmodified>Tue, 13 Oct 2015 17:07:35 GMT</d:getlastmodified>
            <d:getcontentlength>163</d:getcontentlength>
            <d:resourcetype/>
            <d:getetag>"47465fae667b2d0fee154f5e17d1f0f1"</d:getetag>
            <d:getcontenttype>text/plain</d:getcontenttype>
          </d:prop>
          <d:status>HTTP/1.1 200 OK</d:status>
        </d:propstat>
      </d:response>
    </d:multistatus>


.. _KB2668751: https://support.microsoft.com/kb/2668751
.. _KB2123563: https://support.microsoft.com/kb/2123563
.. _in your file manager: http://en.wikipedia.org/wiki/Webdav#WebDAV_client_applications
.. _ownCloud sync clients: https://doc.owncloud.org/desktop/2.1/
.. _Mount ownCloud to a local folder without sync: https://owncloud.org/use/webdav/
.. _Android: https://github.com/owncloud/android
.. _WebDAV Navigator: http://seanashton.net/webdav/
.. _appareils Android: https://play.google.com/store/apps/details?id=com.schimera.webdavnavlite
.. _les iPhones: https://itunes.apple.com/app/webdav-navigator/id382551345
.. _les BlackBerry: http://appworld.blackberry.com/webstore/content/46816
