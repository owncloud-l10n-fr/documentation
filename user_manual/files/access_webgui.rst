=================================================
Accès aux fichiers par l'interface Web d'ownCloud
=================================================

Vous pouvez accéder à vos fichiers ownCloud par l'interface Web et créer, prévisualiser,
modifier, supprimer, partager et re-partager des fichiers. Votre administrateur ownCloud
peut désactiver certaines de ces fonctionnalités. S'il en manque sur votre système, demandez-lui
de les ajouter.

.. figure:: ../images/files_page.png
   :scale: 75%
   :alt: L'écran Fichiers.
   
Étiquetage des fichiers
-----------------------

Nouveau dans la version 9.0 : l'étiquetage de fichiers. Vous pouvez associer des étiquettes à des fichiers. Pour créer des étiquettes,
ouvrez la vue « Détails » d'un fichier. Saisissez alors vos étiquettes. Pour saisir plus d'une étiquette,
appuyez sur la touche « Entrée » après la création de chaque étiquette. Les étiquettes sont globales et partagées
par tous les utilisateurs de votre serveur ownCloud.

.. figure:: ../images/files_page-7.png
   :alt: Création d'étiquette de fichiers.
   
Utilisez le filtre Étiquettes dans la barre latérale de gauche pour filtrer les fichiers en fonction de leurs étiquettes.

.. figure:: ../images/files_page-8.png
   :alt: Affichage des étiquettes de fichiers.
 
Commentaires
------------
 
Nouveau dans la version 9.0 : les commentaires. Utilisez la vue « Détails » pour ajouter et lire des commentaires sur
les fichiers ou les dossiers. Les commentaires sont visibles pour tout utilisateur autorisé à accéder au fichier.

.. figure:: ../images/file_menu_comments_2.png
   :alt: Création et affichage de commentaires.
  
Visionneuse vidéo
-----------------

Vous pouvez lire des vidéos dans ownCloud avec l'application Video Player en cliquant simplement sur le
fichier.

.. figure:: ../images/video_player_2.png
   :alt: Lecture d'une vidéo.
  
Contrôles de fichier
--------------------
   
ownCloud peut afficher des aperçus de fichiers images, de fichiers MP3 ou de fichiers
texte, si cette option est activée par l'administrateur. Passer la souris au-dessus
d'un fichier ou d'un dossier pour exposer les contrôles pour les opérations suivantes :

Favoris
  Cliquez sur l'étoile à gauche de l'icône du fichier pour le marquer en favori et retrouver rapidement 
  tous vos favoris à l'aide du filtre Favoris dans la barre latérale gauche.
  
.. figure:: ../images/files_page-1.png
   :alt: Marquage de fichiers en favoris.
  
Partager
  Partagez le fichier ou le dossier avec un groupe d'utilisateurs ou d'autres utilisateurs et créez des partages
  publics avec des hyperliens. Vous pouvez aussi voir avec qui vous avez déjà partagé le fichier et révoquer 
  des partages en cliquant sur l'icône représentant une corbeille. 
  
.. note:: Nouveau dans la version 9.0, vous pouvez visualiser tous les re-partages de vos partages de fichiers originaux.

  Si l'auto-complétion des noms d'utilisateur est activée, quand vous commencez à saisir
  le nom d'un utilisateur ou d'un groupe, ownCloud le complètera automatiquement 
  pour vous. Si votre administrateur a activé les notifications par courriel,
  vous pouvez envoyer un courriel de notification pour le nouveau partage à partir de
  l'écran de partage.
  
.. figure:: ../images/files_page-2.png
   :alt: Partage de fichiers.
   
Vous disposez de cinq permissions de partage :
 
* **peut partager** : autorise les utilisateurs avec qui vous partagez à re-partager.
* **peut modifier** : autorise les utilisateurs avec qui vous partagez à modifier vos fichiers partagés et à collaborer en
  utilisant l'application Documents.
* **création** : autorise les utilisateurs avec qui vous partagez à créer de nouveaux fichiers et à les ajouter au partage.
* **modification** : autorise le téléversement d'une nouvelle version du fichier partagé et son remplacement.
* **suppression** : autorise les utilisateurs avec qui vous partagez à supprimer les fichiers partagés.

Menu de débordement
  Le menu de débordement (l'icône représentant trois points alignés) affiche les détails du fichier et permet de renommer, 
  de télécharger ou de supprimer les fichiers.
  
.. figure:: ../images/files_page-3.png
   :alt: Menu de débordement.
   
   La vue « Détails » affiche des informations sur l'activité, le partage et les versions. 
  
.. figure:: ../images/files_page-4.png
   :alt: Vue Détails.  
 
Prévisualisation de fichiers
----------------------------

Vous pouvez afficher des fichiers textes non compressés, des fichiers OpenDocument, des vidéos et des images
dans les visionneuses intégrées à ownCloud en cliquant sur le nom du fichier. Il peut y avoir d'autres types de 
fichiers pris en charge su votre administrateur ownCloud les a activés. Si ownCloud ne peut afficher un fichier,
il démarre un processus de téléchargement et télécharge le fichier sur votre ordinateur.

Le streaming vidéo avec la visionneuse intégrée d'ownCloud dépend de votre navigateur Web et du
format vidéo. Si votre administrateur ownCloud a activé le streaming vidéo et qu'il ne fonctionne pas
dans votre navigateur, c'est probablement un problème du navigateur.

Navigation dans ownCloud
------------------------

La navigation dans les dossiers dans ownCloud est aussi simple que de cliquer sur un dossier pour
l'ouvrir et d'utiliser le bouton de retour arrière de votre navigateur pour revenir à la page précédente. 
ownCloud propose aussi une barre de navigation dans la partie supérieure de la vue Fichiers pour naviguer
rapidement.

Icônes d'état de partage
------------------------

Tout dossier partagé est marqué avec l'icône ``Partagé`` en surimpression. 
Les liens de partages publics sont marqués par une icône représentant une chaîne en surimpression. Les dossiers non partagés
n'ont pas d'icône en surimpression.

.. figure:: ../images/files_page-5.png
   :alt: Icônes d'état de partage.

Si votre serveur ownCloud est une Souscription Enterprise, vous pouvez aussi accéder
à des partages Sharepoint ou de lecteurs réseau Windows. Ceux-ci disposent d'icônes d'état spéciales.
Une icône avec une prise rouge signifie que vous devez vous identifier pour accéder au partage.

.. figure:: ../images/users-overlays-win-net-drive.png

.. figure:: ../images/users-overlays-sharepoint.png

Création ou téléversement de fichiers et de répertoires
-------------------------------------------------------

Téléverser ou créer de nouveaux fichiers et répertoires directement dans ownCloud en cliquant 
sur le bouton *Nouveau* dans l'application Fichiers.

.. figure:: ../images/files_page-6.png
   :alt: Le menu nouveau fichier/répertoire/téléversement.

Le bouton *Nouveau* propose les options suivantes :

Chargement
  Téléverse les fichiers de votre ordinateur vers ownCloud. Vous pouvez aussi téléverser des fichiers en les glissant et
  les déposant depuis votre gestionnaire de fichiers (nécessite Chrome ou Chromium).

Fichier texte
  Crée un nouveau fichier texte et l'ajoute dans le dossier courant.
  
Dossier
  Crée un nouveau dossier dans le dossier courant.
  
Sélection de fichiers ou de dossiers
------------------------------------

Vous pouvez sélectionner un ou plusieurs fichiers ou répertoires en cochant les cases à cocher (qui s'affichent
quand on passe la souris au-dessus de leur nom). Pour sélectionner tous les fichiers du répertoires courant, 
cochez la case située en haut de la liste des fichiers.

Quand vous sélectionnez plusieurs fichiers, vous pouvez tous les supprimer ou les técharger sous forme d'un fichier
ZIP en utilisant les boutons ``Supprimer`` ou ``Télécharger`` qui apparaissent dans le coin supérieur droit
de la fenêtre.

.. note:: Si le bouton ``Télécharger`` n'est pas visible, l'administrateur a désactivé
   cette fonctionnalité.

Filtrer la vue Fichiers
-----------------------

La barre latérale gauche de la page Fichiers contient plusieurs filtres pour trier et gérer rapidement
vos fichiers.

Tous les fichiers
  C'est la vue par défaut. Elle affiche tous les fichiers auxquels vous avez accès.
  
Favoris
  Les fichiers ou dossiers marqués par une étoile jaune. 

Partagés avec vous
  Tous les fichiers partagés avec vous par un autre groupe ou utilisateur.

Partagés avec d'autres
  Tous les fichiers que vous avez partagés avec d'autres utilisateurs ou groupes.

Partagés par lien
  Tous les fichiers qui sont partagés à l'aide d'un lien public.
  
Stockage externe
  Tous les fichiers auxquels vous pouvez accéder sur des équipements de stockage externes ou des services tels que
  Dropbox, Google et Amazon S3.

Étiquettes
  Permet de filtrer les fichiers en fonctions de leurs étiquettes.

Déplacement de fichiers
-----------------------

Vous pouvez déplacer des fichiers et des répertoires en les glissant et en les déposant sur un dossier.


Modification de la date d'expiration d'un partage
-------------------------------------------------

Dans les anciennes versions d'ownCloud, vous pouviez définir une date d'expiration sur les partages locaux
et les partages publics. Désormais, vous ne pouvez définir de date d'expiration que sur les partages publics, 
et le partage local n'expire pas quand le partage public expire. Le seul moyen pour faire expirer un partage local
est de cliquer sur l'icône représentant une corbeille pour stopper le partage de vos fichiers.

Création ou connexion à un lien de partage fédéré
-------------------------------------------------

Le partage fédéré permet de monter des partages de fichiers entre plusieurs serveurs ownCloud
et de les gérer comme s'il s'agissait d'un partage local. Dans ownCloud 9, le processus de création
d'un nouveau lien de partage est plus simple et intuitif. Consulter 
:doc:`federated_cloud_sharing` pour savoir comment créer et se connecter à de nouveaux partages
fédérés.
