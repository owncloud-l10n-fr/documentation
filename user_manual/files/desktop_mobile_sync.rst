====================================
Synchronisation ordinateur et mobile
====================================

Pour synchroniser les fichiers avec votre ordinateur, nous recommandons d'utiliser le
`Client de synchronisation ownCloud`_ pour Windows, Mac OS X et GNU/Linux.

Le client de synchronisation ownCloud vous permet de vous connecter à votre serveur
ownCloud privé.
Vous pouvez créer des dossiers dans votre répertoire de synchronisation et garder le contenu de ces
dossiers synchronisés avec votre serveur ownCloud. Il suffit de copier un fichier dans le répertoire
et le client ownCloud s'occupe du reste. Faites une modification sur un fichier dans un ordinateur
et la modification sera répercutée sur tous les autres ordinateurs utilisant le client de synchronisation ownCloud. 
Vous aurez toujours la dernière version de vos fichiers où que vous vous trouviez.

Son utilisation est documentée séparément dans le `Manuel du client ownCloud pour ordinateur`_.

.. _Client de synchronisation ownCloud : https://owncloud.org/sync-client/
.. _Manuel du client ownCloud pour ordinateur :  https://doc.owncloud.org/

Clients pour mobiles
--------------------

Veuillez visiter votre page personnelle dans l'interface Web d'ownCloud pour trouver les liens de téléchargement
pour les clients de synchronisation pour mobiles Android et iOS. Vous pouvez aussi vous rendre sur la `page des téléchargements d'ownCloud 
<https://owncloud.org/install/>`_.

Consulter la `page de documentation ownCloud <https://doc.owncloud.org/>`_ pour lire les manuels 
utilisateur pour les applications pour mobiles.
