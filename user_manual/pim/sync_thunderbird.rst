Thunderbird - Synchronisation du carnet d'adresses
==================================================

Carnet d'adresses
-----------------

Si vous découvrez ownCloud, le connecteur SoGo et le carnet d'adresses de Thunderbird... voici ce que vous devez faire :

#. `Thunderbird <http://www.mozilla.org/en-US/thunderbird/>`_ de votre système d'exploitation à moins qu'il ne soit fourni dans votre distribution (GNU/Linux)
#. `Connecteur Sogo <http://www.sogo.nu/downloads/frontends.html>`_ (dernière version)
#. `Lightning <https://addons.mozilla.org/en-US/thunderbird/addon/lightning/>`_ (une extension Agenda pour Thunderbird.
   À ce jour (14/08/2015), la synchronisation de vos contacts ne fonctionne qu'avec cette extension installée).

Avec tous ces outils installés :

#. Le carnet d'adresses de Thunderbird est accessible à partir du menu « Outils ».
#. Dans l'application Carnet d'adresses de Thunderbird :

   -  « Fichier > Nouveau > **Carnet d'adresses distant** » (le connecteur SoGo a ajouté ce menu)
   -  « **Nom :** » est le nom que vous voulez donner à ce carnet d'adresses qui s'affichera dans la zone des carnets de Thunderbird
   -  « **URL :** » qui se trouve dans la zone Contacts d'ownCloud

.. image:: ../images/contact_thunderbird-Symbol_Gear.jpg

en cliquant sur l'icône représentant un petit engrenage, en bas à gauche de la fenêtre. Recherchez une icône représentant une petite roue

.. image:: ../images/contact_thunderbird-Symbol_Impeller.jpg

qui affichera l'URL dont vous avez besoin pour que votre installation fonctionne.

.. image:: ../images/contact_thunderbird-URL_config.jpg

Une fois installé, synchronisez (en faisant un clic droit sur votre nouveau carnet d'adresses distant et en sélectionnant « Synchroniser »).
Vous verrez alors votre carnet d'adresses alimenté à partir d'ownCloud ! Ne cochez pas « Lecture seule » en dessous, sauf si vous ne voulez
pas modifier votre carnet d'adresses sur le serveur ownCloud, car il contient une liste de contacts professionnels et qu'il est partagé
avec beaucoup de gens et que vous ne voulez pas qu'un nouvel utilisateur la déplace.

Les détails de la gestion du carnet d'adresses de Thunderbird sont laissés au lecteur... La première chose que j'ai apprise est que
faire glisser un contact dans un autre carnet d'adresses correspond à une opération de déplacement et pas de copie. Si vous avez peur de perdre
le contact, enregistrez-le d'abord dans un fichier VCF en utilisant ownCloud (ou un fichier LDIF en utilisant le carnet d'adresses de Thunderbird) !
De même, faire une opération de glisser-déposer à partie du carnet d'adresses de ownCloud vers votre carnet d'adresses personnel,
supprime le contact du serveur ownCloud (*en le supprimant de toutes les autres installations synchronisées*) en le mettant uniquement dans
le carnet d'adresses de votre ordinateur local. Faites donc très attention ou vous aurez des conséquence inattendues alors que vous pensiez
faire une opération de copie.

Les *images* des contacts sont également synchronisées !
