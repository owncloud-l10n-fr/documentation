===========================
Synchronisation avec KDE SC
===========================

.. image:: ../images/kdes1.png

À compter de la version 4.8 de KDE SC, paramétrer ownCloud est très simple. Veuillez
noter que KDE nécessite que les applications Agenda et Contacts soient activées
sur le serveur ownCloud. Les deux applications doivent être activées et pas uniquement l'application Agenda.
Dans le menu « Paramètres système > Informations personnelles > Configuration des ressources Akonadi »,
sélectionnez « Ressource groupware DAV ».

.. image:: ../images/kdes2.png

Saisissez vos nom d'utilisateur et mot de passe ownCloud, puis cliquez sur « Next ».


.. image:: ../images/kdes3.png

Sélectionnez ownCloud dans la liste déroulante et cliquez sur « Next ».



.. image:: ../images/kdes4.png

Saisissez le nom d'hôte et le chemin d'installation. Si vous n'utilisez pas SSL,
n'oubliez pas de désélectionner « Utiliser une connexion sécurisée ».


.. image:: ../images/kdes5.png

Testez la connexion. Si tout s'est bien passé, vous devriez voir un message comme celui
ci-dessous.


.. image:: ../images/kdes6.png

Cliquez sur « Terminer » et vous devriez être en mesure de modifier le nom
d'affichage et l'intervalle d'actualisation.


.. image:: ../images/kdes7.png

Vous devriez maintenant voir la ressource Akonadi en réalisant la première
synchronisation.

..  missing
..  .. image:: ../images/kdes8.png

Vous pouvez trouver les contacts et les agendas dans Kontact (ou dans
KOrganizer et KAddressbook si vous utilisez les programmes séparément).


.. image:: ../images/kdes9.png

.. image:: ../images/kdes.png
