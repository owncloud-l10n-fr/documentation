==================
Contacts & agendas
==================

Les applications Contacts, Calendar et Mail ne sont pas incluses dans ownCloud 9 et ne sont pas 
supportées. Vous pouvez facilement les installer en cliquant sur le bouton « Activer » dans leurs sections
respectives dans Applications > Productivity.

.. toctree::
   :maxdepth: 1
   
   contacts
   calendar
   sync_ios
   sync_osx
   sync_kde
   sync_thunderbird
   troubleshooting

