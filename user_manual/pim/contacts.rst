=====================================
Utilisation de l'application Contacts
=====================================

L'application Contacts n'est pas activée par défaut dans ownCloud |version| et doit
être activée séparément. Elle ne fait pas non plus partie des applications de base supportées.
Vous pouvez demander à votre administrateur ownCloud de l'activer, et si vous êtes votre
propre administrateur, consultez la section `Installation > Installation et gestion des applications
<https://doc.owncloud.org/server/9.0/admin_manual/installation/
apps_management_installation.html>`_
du `manuel administrateur d'ownCloud
<https://doc.owncloud.org/server/9.0/admin_manual/html>`_.

L'application Contacts d'ownCloud est similaire aux autres applications contacts pour mobile, mais 
dispose de plus de fonctionnalités. Lors du premier accès à l'application Contacts, un carnet d'adresses par
défaut devient disponible.

.. figure:: ../images/contacts_empty.png
   :alt: Écran principal de l'application Contacts.

Dans le champ d'information de l'application Contacts, vous pouvez choisir de créer un nouveau
contact ou un nouveau groupe. Ce champ permet également de filtrer les contacts en fonction de
leur appartenance ou non à des groupes.

Ajout de contacts
-----------------

Vous pouvez ajouter des contacts en utilisant une des méthodes suivantes :

* Import des contacts en utilisant un fichier au format Variant Call (VCF).

* Ajout des contacts manuellement.

Import de contacts
~~~~~~~~~~~~~~~~~~

Le moyen le plus rapide d'ajouter des contacts est de le faire avec un fichier au format
Variant Call (VCF).

Pour importer des contacts en utilisant un fichier VCF :

1. Au bas du champ d'information de l'application Contacts, repérez le bouton représentant un engrenage.

  .. figure:: ../images/contact_bottombar.png
     :alt: Bouton représentant un engrenage pour le paramétrage de Contacts.

2. Cliquez sur ce bouton. Le champ de chargement de l'application Contacts s'ouvre :

  .. figure:: ../images/contact_uploadbutton.png
     :alt: Champ de chargement de l'application Contacts.

3. Choisissez un carnet d'adresses vers lequel importer les contacts en cochant la case à côté
du carnet d'adresses.

4. Sélectionnez une des options dans le menu déroulant « Import ». Ces options
sont les suivantes :

  - Format automatique -- Tout fichier VCF. ownCloud détermine le format du fichier et l'importe
    en conséquence ;
	
  - CSV Gmail -- Fichier avec valeurs séparées par des virgules de votre compte Gmail ;
  
  - CSV Outlook -- Fichier avec valeurs séparées par des virgules de votre compte Outlook ;
  
  - CSV Thunderbird -- Fichier avec valeurs séparées par des virgules de votre compte Thunderbird ;
	
  - CSV Yahoo -- Fichier avec valeurs séparées par des virgules de votre compte Yahoo ;
  
  - Export ldif PHPLdapAdmin -- Le fichier d'export de votre configuration LDAP ;
  
  - VCard Gmail -- Les fichiers VCard de votre compte Gmail ;
  
  - VCard Standard -- Les fichiers VCard de votre compte Standard ;
	
  - VCard Yahoo -- Les fichiers VCard de votre compte Yahoo ;
   
5. Cliquez sur la flèche de chargement à droite de l'option d'import que vous avez choisie.

   Une fenêtre de chargement de fichiers s'ouvre.
  
6. Sélectionnez le fichier approprié sur votre système.

  .. note:: Vous pouvez charger les fichiers individuellement, ou, en sélectionnant plusieurs fichiers
    en utilisant la touche CTRL, vous pouvez charger plusieurs fichiers en même temps.

7. Cliquez alors sur le bouton ``Ouvrir``.

  .. figure:: ../images/contact_vcfpick.jpg
     :alt: Sélection de fichiers VCF.

L'interface place alors automatiquement vos contacts dans ownCloud.

Création manuelle de contacts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'application Contacts permet de créer les contacts manuellement.  

Pour créer un nouveau contact :

1. Cliquez sur « Ajouter un nouveau contact ».

   Une fiche de nouveau contact vierge s'ouvre :
  
  .. figure:: ../images/contact_new.png
     :alt: Ajout d'un nouveau contact.
  
2. Spécifiez les informations du nouveau contact comme suit :

  - Nom -- Le nom du contact. Ce champ fournit l'option d'ajouter
    les informations pour l'utilisateur en cliquant sur l'icône représentant un crayon
    à droite du champ nom.
	
  - Société -- La société du contact.
  
  - Adresse e-mail -- L'adresse électronique du contact. Ce champ est par défaut l'adresse
    professionnelle du contact. Cependant, vous pouvez spécifier une désignation différente
    pour cette adresse en cliquant sur la désignation ``Pro`` à gauche du champ.
	
  - Téléphone -- Le numéro de téléphone du contact. Ce champ est par défaut le numéro
    privé du contact. Cependant, vous pouvez spécifier une désignation différente pour
    ce numéro en cliquant sur la désignation ``Privé(e)`` à gauche du champ.
	
  - Adresse -- L'adresse postale du contact. Ce champ est par défaut l'adresse
    professionnelle du contact. Cependant, vous pouvez spécifier une désignation différente
    pour cette adresse en cliquant sur la désignation ``Pro`` au-dessus du champ.
	
  - Notes -- Toutes notes que vous voulez ajouter pour le contact.
  
  .. note:: Les champs Adresse e-mail, Téléphone et Adresse fournissent l'option de spécifier
    une méthode de contact « préférée ».
  
Modification des informations de contact
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'application Contacts permet de modifier ou de supprimer des informations de contact.

Pour modifier les informations de contact :

1. Sélectionnez le contact que vous souhaitez modifier.

2. Choisissez l'information que vous voulez modifier.

3. Faites les modifications.

  Les modifications faites sur les contacts sont prises en compte immédiatement.
  
Suppression d'une information de contact
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'application Contacts permet de supprimer des informations de contact.

Pour supprimer une information de contact :

1. Sélectionnez le contact que vous souhaitez modifier.

2. Choisissez l'information que vous voulez supprimer.

3. Cliquez sur l'icône représentant une corbeille à droite du contact.

  Les modifications faites sur les contacts sont prises en compte immédiatement.


Définition de l'avatar d'un contact
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Par défaut, les nouveaux contacts reçoivent comme avatar (image) une lettre « U » (pour « Utilisateur »).

.. figure:: ../images/contact_picture_default.png
   :alt: Image par défaut du contact.

Quand vous spécifiez un nom pour le contact, l'avatar reflète le nom en adoptant
la première lettre du nom fourni. Par exemple, si vous indiquez
le prénom « Frédéric », l'avatar changera dynamiquement pour la lettre « F » pour
ce contact. Si vous fournissez plusieurs contacts ayant le même nom ou dont le nom
commencent par la même lettre, l'avatar utilise la même lettre mais sa couleur change
pour indiquer la différence.
 
.. figure:: ../images/contact_picture.png
   :alt: Nouvelle image de contact.

Personnalisation d'un avatar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En plus de modifier dynamiquement l'avatar pour chaque contact, l'application Contacts 
permet de personnaliser l'avatar. Vous pouvez spécifier un avatar de deux façons
différentes :

- En téléversant une nouvelle image -- En sélectionnant cette option, ownCloud ouvre une fenêtre de
  chargement de fichier. Vous pouvez choisir une nouvelle image en la sélectionnant et en cliquant sur
  le bouton ``Ouvrir``.
  
- En sélectionnant une image dans l'application Fichiers -- En sélectionnant cette option, ownCloud ouvre le dialogue Fichiers
  sur le serveur ownCloud. Vous pouvez choisir une image en parcourant le répertoire dans
  ownCloud, en la sélectionnant et en cliquant sur ``Choisir.``

Découpage d'une image d'avatar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Après avoir sélectionné une image pour votre contact, l'application Contacts vous permet de
découper l'image.

.. figure:: ../images/contact_crop.jpg
   :alt: Découpage de l'image d'un contact.

Pour découper l'image :

1. Déplacez la boîte de découpage à l'endroit désiré et redimensionnez-la selon vos besoins.

2. Cliquez sur ``Découper l'image`` dans la boîte de dialogue.

  L'application Contacts découpe l'image et remplace l'image précédemment utilisée
  pour le contact.

Gestion du carnet d'adresses
----------------------------

Cliquez sur le bouton de paramètres (icône représentant un engrenage) en bas à gauche de la fenêtre
pour accéder aux paramètres de l'application Contacts. Ce champ affiche tous les carnets d'adresses
disponibles, certaines options pour chaque carnet et permet de créer de nouveaux carnets
d'adresses.

.. figure:: ../images/contacts_settings.png
   :alt: Paramètres de l'application Contacts.

Les paramètres de l'application Contacts permettent de partager, exporter modifier et
supprimer les carnets d'adresses.

.. note:: Passer le curseur de la souris au-dessus de chaque icône pour voir une brève description.

Ajout d'un carnet d'adresses
----------------------------

Pour ajouter un carnet d'adresses :

1. Cliquez sur ``+ Nouveau carnet d'adresses`` dans le champ de paramètres de l'application Contacts.

  Un champ s'affiche invitant à saisir un nom d'affichage pour le nouveau carnet d'adresses.
  
  .. figure:: ../images/contact_address_book_add.png
     :alt: Ajout d'un nouveau carnet d'adresses.


2. Indiquez un nom d'affichage pour le carnet d'adresses.

3. Cochez la case pour créer le nouveau carnet d'adresses.


Synchronisation des carnets d'adresses
--------------------------------------

Une des fonctionnalités les plus importantes dans toute application de gestion de contacts est la possibilité de les
garder synchronisés. L'application Contacts d'ownCloud vous permet de synchroniser vos carnets d'adresses avec des équipements
externes qui utilisent les systèmes d'exploitation Android ou Apple iOS.


Synchronisation avec Android
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour faire une synchronisation avec un appareil Android :

1) Installez l'application CardDAV - Sync (gratuite) du Google Play Store en visitant `ce lien
   <https://play.google.com/store/apps/details?id=org.dmfs.carddav.sync>`_.
   Cette application gère la configuration automatique.
  
  .. note:: Après l'installation, visitez l'adresse carddavs://exemple.com/remote.php/dav/
     pour configurer automatiquement l'application (remplacez « exemple.com » par le nom de domaine de votre serveur ownCloud).

3) Saisissez vos identifiants de connexion.

4) Quand l'application a fini de vérifier vos identifiants de connexion, sélectionner l'option ``Synchroniser du serveur vers le téléphone seulement``.

.. figure:: ../images/contact_syncopt.jpg

Synchronisation avec Apple iOS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour faire une synchronisation avec un appareil Apple iOS :

1. Ouvrez l'application des paramètres.

2. Sélectionnez Mail > Contacts > Agendas.

3. Sélectionnez ``Ajouter un compte``.

4. Sélectionnez ``Autre`` pour le type de compte.

5. Sélectionnez ``Ajouter un compte CardDAV``.

6. Pour ``serveur``, saississez https://exemple.com/remote.php/dav/principals/utilisateur

7. Indiquez vos noms d'utilisateur et mot de passe.

8. Sélectionnez Suivant.

9. Si votre serveur ne gère pas SSL, un avertissement est affiché. Sélectionnez ``Continuer``.

10. Si l'iPhone n'arrive pas à vérifier les informations de connexion, effectuez les actions suivantes :

  a. Cliquez sur ``OK``.
  
  b. Sélectionnez ``paramètres avancés``.

  c. Assurez-vous que ``Utiliser SSL`` est défini « OFF ».

  d. Changez le port pour la valeur ``80``.
  
  e. Revenez sur les informations de compte et cliquez sur ``Enregistrez``.

  Vos contacts apparaissent alors dans le carnet d'adresses de votre iPhone.


Utilsiation d'autres options de synchronisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ownCloud propose les options de synchronisation alternatives suivantes :

- Pour les appareils Android, vous pouvez utiliser l'application Android officielle. L'application se
  trouve `ici <https://owncloud.org/install/>`_.
  
- Pour les appareils iOS (iPhone et iPad), vous pouvez utiliser l'application Android officielle. L'application se
  trouve `ici <https://owncloud.org/install/>`_.

URL spéciales CardDAV
---------------------

De plus, l'application Contacts fournit une URL pour les fonctions spéciales :

**Exporter un carnet d'adresses sous forme de fichier vCard**

  https://exemple.com/remote.php/dav/addressbooks/UTILISATEUR/NOMCARNETDADRESSES?export

Dépannage
---------

Des problèmes pour utiliser l'application ? Consulter les guides :doc:`troubleshooting`
et `Dépannage de Contacts & Agendas`_.

.. _Dépannage de Contacts & Agendas: https://doc.owncloud.org/server/9.0/admin_manual/issues/index.html#troubleshooting-contacts-calendar