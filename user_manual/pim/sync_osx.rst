Synchronisation avec OS X
=========================

Pour utiliser ownCloud avec iCal, vous devez utiliser l'URL suivante::

    https://exemple.com/remote.php/dav/principals/utilisateur/

Le paramétrage est pratiquement le même que celui d'iOS en utilisant le chemin ``https://exemple.com/remote.php/dav/principals/utilisateur/``
pour se synchroniser avec ownCloud. Pour OS X 10.7 Lion et 10.8 Mountain Lion, tout fonctionne
correctement, mais pour OS X 10.6 (Snow Leopard) et les versions précédentes, vous devrez bidouiller un peu
pour que cela fonctionne. Un utilisateur a proposé ce qui suit :

#. Assurez-vous que le carnet d'adresse n'est pas en cours de fonctionnement. Si c'est le cas, séléctionnez la fenêtre
et appuyez sur les touches « Command + Q » pour l'arrêter.
#. Rendez-vous dans le répertoire **/Users/UTILISATEUR/Library/Application Support/AddressBook/Sources**.
Si vous avez déjà un paramétrage du carnet d'adresses, vous devriez voir des dossiers
de ce type : **BEA92826-FBF3-4E53-B5C6-ED7C2B454430**.
Notez le nom de ces dossiers et laissez la fenêtre ouverte.
#. Ouvrez l'application Carnet d'adresses et ajoutez un nouveau carnet CardDav. Pour le moment, peu importe les
informations que vous saisissez. Il apparaîtra le même message d'erreur mentionné plus tôt quand vous
cliquez sur « Créer ». Ignorez ce message et cliquez sur « Créer» à nouveau.
Un carnet d'adresse non opérationnel sera ajouté.
#. Fermez à nouveau l'application Carnet d'adresse en utilisant « Command + Q ».
#. Retournez dans la fenêtre de l'étape 2. Vous voyez alors un nouveau dossier avec une longue chaîne de
caractères comme nom.
#. Rendez-vous dans ce nouveau dossier et éditer le fichier **Configuration.plist** avec votre
éditeur de texte préféré.
#. Recherchez la section ressemblant à ceci::

    <key>servername</key> <string>https://:0(null)</string> <key>username</key> <string>Ce_que_vous_avez_saisi_précédemment</string>

8. Modfiez cette section pour qu'elle ressemble à ce qui suit. Veuillez noter que « :443 » après **exemple.com** est important::

    <key>servername</key <string>https://exemple.com:443/owncloud/remote.php/dav/principals/utilisateur</string> <key>username</key> <string>utilisateur</string>

9. Enregistrez le fichier et ouvrez le carnet d'adresses à nouveau. Il ne fonctionne pas encore.

10. Ouvrez les préférences de votre compte ownCloud CardDAV et saisissez votre mot de passe.

11. Vous pourriez avoir besoin de redémarrer l'application Carnet d'adresses encore une fois. Après cela, cela devrait fonctionner.

Si cela ne fonctionne toujours pas, consultez les guides :doc:`troubleshooting` et
`Dépannage de Contacts & Agendas`_.

Il existe aussi un `guide`_ simple dans le forum.


.. _guide: https://forum.owncloud.org/viewtopic.php?f=3&t=132
.. _Dépannage de Contacts & Agendas: https://doc.owncloud.org/server/9.0/admin_manual/issues/index.html#troubleshooting-contacts-calendar
