=========
Dépannage
=========

BlackBerry OS 10.2
------------------

BlackBerry OS jusqu'à la version 10.2.2102 n'accepte pas d'URL avec le protocole ``https://`` 
devant l'adresse du serveur. Il indiquera toujours qu'il ne peut pas se connecter à votre
serveur. Aussi, au lieu d'écrire::

    https://exemple.com/remote.php/dav/principals/utilisateur
    
dans le champ d'adresse du serveur, vous devrez écrire::

    exemple.com/remote.php/dav/principals/utilisateur
    