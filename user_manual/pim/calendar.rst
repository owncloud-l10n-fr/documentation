===================================
Utilisation de l'application Agenda
===================================

L'application Agenda n'est pas activée par défaut dans ownCloud |version| et doit
être activée séparément. Elle ne fait pas non plus partie des applications de base supportées.
Vous pouvez demander à votre administrateur ownCloud de l'activer, et si vous êtes votre
propre administrateur, consultez la section `Installation > Installation et gestion des applications
<https://doc.owncloud.org/server/9.0/admin_manual/installation/
apps_management_installation.html>`_
du `manuel administrateur d'ownCloud
<https://doc.owncloud.org/server/9.0/admin_manual/html>`_.

L'application Agenda d'ownCloud permet de créer et de modifier des événements, de les synchroniser
avec d'autres agendas que vous pourriez utiliser, et de créer de nouveaux agendas personnalisés.

Par défaut, lors du premier accès à l'application Agenda, vous obtenez un agenda
« default » que vous pouvez utiliser ou modifier selon vos besoins.

.. figure:: ../images/calendar_default.png
   :alt: Écran principal de l'application Agenda.
   
L'application Agenda contient les champs et contrôles suivants :

- Champ date -- Fournit la date courante ou permet de sélectionner une date.

- Options de vues -- Permet de choisir la vue jour, semaine ou mois dans la
  fenêtre principale de l'application Agenda. Propose également un bouton d'accès
  rapide au jour courant (Aujourd'hui).

- Sélection -- Permet de choisir l'agenda à afficher.

- Contrôles -- Fournit des contrôles pour chaque agenda. Ces contrôles permettent
  le partage, l'obtention d'une URL externe pour accéder à l'agenda, l'export,
  la modification ou la suppression.

  .. note:: Certains bloqueurs de publicité masquent le bouton « Share » des agendas. Si ce
     bouton n'apparaît pas, désactivez le bloqueur de publicité ou ajoutez votre instance
     ownCloud en liste blanche.

- Paramètres -- Fournit l'accès aux paramètres spécifiques des agendas. Ces paramètres comprennent
  la sélection du fuseau horaire, le choix du format de date, le jour de début de la
  semaine, le paramètre de cache, l'adresse primaire CalDAV et les paramètres d'adresse CalDAV pour iOS/OS X.

Création d'un nouvel agenda
---------------------------

L'application Agenda permet de créer de nouveaux agendas pour le travail et d'autres activités
que vous voulez tenir séparément. Vous pouvez gérer chaque agenda individuellement.

Pour créer un nouvel agenda :

1. Accédez à l'application Agenda.

2. Cliquez sur ``+ Nouvel agenda``.

   Un dialogue s'ouvre permettant de créer le nouvel agenda.

.. figure:: ../images/calendar_create_new.png

  **Nouvel agenda**

3. Spécifiez un nom pour le nouvel agenda.

4. (Facultatif) Indiquez une couleur pour l'agenda.

5. Cochez la case à cocher bleue.

  L'application Agenda crée un nouvel agenda utilisant le nom et la couleur indiqués.

Gestion des paramètres d'agenda
-------------------------------

Les paramètres de l'application Agenda proposent une configuration globale s'appliquant
à tous les agendas.

  .. figure:: ../images/calendar_settings.png
     :alt: Paramètres de l'application Agenda.

Dans les paramètres de l'application Agenda, les éléments suivants peuvent être modifiés :

- Fuseau horaire -- Fournit une liste alphabétique de tous les pays disponibles
  classés par continent.

- Format d'heure -- Fournit l'option d'affichage de l'heure au format 24 heures ou 12 heures.

- Premier jour de la semaine -- Permet de choisir le premier jour de la semaine : lundi,
  dimanche ou samedi.

- Adresse primaire CalDAV -- Fournit l'URL de l'adresse primaire CalDAV.

- Adresse CalDAV iOS/OS X -- Fournit l"URL de l'adresse CalDAV iOS/OS X.


Synchronisation des agendas en utilisant CalDAV
-----------------------------------------------
Les *extensions d'agenda de WebDAV*, appelées *CalDAV*, permettent aux clients
d'accéder aux informations de planning sur des serveurs distants. En tant qu'extension de WebDAV,
CalDAV (défini par la RFC 4791) utilise le format iCalendar pour gérer les données d'agenda.
CalDAV permet à plusieurs clients d'accéder à la même information à utiliser dans un contexte de
planning coopératif ou de partage d'informations.

L'apllication Agenda fournit l'adresse primaire CalDAV et l'adresse CalDAV pour iOS/OSX.
En utilisant ces adresses, vous pouvez utilisez des programmes compatibles CalDAV (par
exemple, Kontact, Evolution ou Mozilla Thunderbird avec l'extension Lightning)
en utilisant l'adresse fournie.

.. Note:: L'extension Lightning ne gère pas l'utilisation de plusieurs comptes sur un même
   serveur par défaut. Vous devez définir le paramètre ``calendar.network.multirealm`` à ``true``
   dans l'éditeur de configuration de Mozilla Thunderbird pour être en mesure d'utiliser plusieurs comptes.

Obtention du lien CalDAV
~~~~~~~~~~~~~~~~~~~~~~~~

L'interface graphique d'ownCloud fournit les liens nécessaires qui peuvent être utilisés dans votre
programme compatible CalDAV. Ouvrez l'application Agenda et utilisez l'icône |caldavlink| 
à côté du nom de l'agenda comme illustré ci-dessous :

.. figure:: ../images/calendar_caldav_link.png
   :alt: Lien Caldav.

De plus, vous pouvez obtenir l'adresse CalDAV primaire ainsi que l'adresse CalDAV pour iOS/OS X CalDAV
en utilisant l'icône |gear| représentant un engrenage dans le coin inférieur gauche de l'application
Agenda, comme indiqué ci-dessous :

.. figure:: ../images/calendar_caldav_ios.png
   :alt: 

Création d'événements
---------------------

L'application Agenda permet de planifier des événements, d'envoyer des 
invitations et de définir des rappels.

Pour créer un nouvel événement :

1. Cliquez sur une date dans la vue mois, ou sur une heure dans les vues semaine ou jour.

   La boîte de dialogue ``Nouvel événement`` s'ouvre.

   .. figure:: ../images/calendar_create_event.png
      :alt: Création d'un nouvel événement.

2. Indiquez un titre pour l'événement.

3. Indiquez une date et la durée de l'événement.

4. (Facultatif) Option de détails avancés. Ces détails comprennent le lieu de l'événement,
   la catégorie de l'événement et sa description.

5. (Facultatif) Répétition de l'événement, invitation des participants et définition des rappels.

Export et import d'événements
-----------------------------

L'application Agenda permet d'exporter et d'importer un événement ou un agenda entier à partir
d'autres agendas.

Export d'événements et d'agendas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vous pouvez exporter un événement unique ou tout un agenda. Si vous voulez exporter un unique événement,
cliquez sur l'événement, sélectionnez « Plus... » et cliquez sur le bouton « Exporter » dans le coin inférieur
droit. Si vous voulez exporter un agenda entier, utilisez l'icône représentant une flèche vers le bas à côté
du nom de l'agenda.

Import d'événements
~~~~~~~~~~~~~~~~~~~

Vous pouvez importer votre agenda sous la forme d'un fichier iCal en utilisant l'application Fichiers.
L'application Agenda permet d'importer l'agenda dans un nouvel agenda ou un agenda existant.

Pour importer l'agenda, cliquez sur le fichier d'agenda (iCal) pour ouvrir la boîte de dialogue d'import.

URL spéciales CalDAV
--------------------

L'application Agenda fournit également deux URL pour des fonctions spéciales :

**Exporter l'agenda sous forme de fichier .ics**

  https://exemple.com/remote.php/dav/calendars/UTILISATEUR/NOMAGENDA?export

**Les anniversaires de vos contacts**

  https://exemple.com/remote.php/dav/calendars/UTILISATEUR/contact_birthdays

FAQ de l'application Agenda
---------------------------

**Question :** Pourquoi l'application Agenda requiert-elle ma localisation actuelle ?

.. figure:: ../images/calendar_newtimezone1.png

  **Notification du fuseau horaire**

**Réponse :** L'application Agenda a besoin de connaître votre localisation pour déterminer votre fuseau horaire.
Si le fuseau horaire n'est pas correct, il y aura un décalage horaire entre les événements de l'agenda
sur le serveur ownCloud et l'agenda de l'ordinateur à partir duquel vous faites la synchronisation. Vous pouvez
également définir le fuseau horaire manuellement dans vos paramètres personnels.

.. |download| image:: ../images/download.png
.. |caldavlink| image:: ../images/calendar_caldav_icon.png
.. |gear| image:: ../images/gear.png
