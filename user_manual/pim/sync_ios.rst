iOS - Synchronisation iPhone/iPad
=================================

Agenda
------

#. Ouvrez l'application des paramètres.
#. Sélectionnez « Mail, contacts, agendas ».
#. Sélectionnez « Ajouter un compte ».
#. Sélectionnez « Autre » comme type de compte.
#. Sélectionnez « Ajouter un compte CalDAV ».
#. Pour « Serveur », saisissez ``exemple.com/remote.php/dav/principals/utilisateur``.
#. Saisissez votre nom d'utilisateur et votre mot de passe.
#. Sélectionnez « Suivant ».
#. Si votre serveur ne gère pas le protocole SSL, un avertissement sera affiché.
   Sélectionnez « Continuer ».
#. Si l'iPhone n'arrive pas à vérifier les informations de compte,
   effectuez les opérations suivantes :

   -  sélectionnez « OK » ;
   -  sélectionnez les paramètres avancés ;
   -  si votre serveur ne gère pas le protocole SSL, assurez-vous que « Utilisez SSL » soit défini à « OFF » ;
   -  changez-le pour la valeur « 80 » ;
   -  revenez aux informations de compte et sauvegardez.

Votre agenda est maintenant visible dans l'application Agenda.


Carnet d'adresses
-----------------

#. Ouvrez l'application des paramètres.
#. Sélectionnez « Mail, contacts, agendas ».
#. Sélectionnez « Ajouter un compte ».
#. Sélectionnez « Autre » comme type de compte.
#. Sélectionnez « Ajouter un compte CalDAV ».
#. Pour « Serveur », saisissez ``exemple.com/remote.php/dav/principals/utilisateur``.
#. Saisissez votre nom d'utilisateur et votre mot de passe.
#. Sélectionnez « Suivant ».
#. Si votre serveur ne ne gère pas le protocole SSL, un avertissement sera affiché.
   Sélectionnez « Continuer ».
#. Si l'iPhone n'arrive pas à vérifier les informations de compte,
   effectuez les opérations suivantes :

   -  sélectionnez « OK » ;
   -  sélectionnez les paramètres avancés ;
   -  si votre serveur ne gère pas le protocole SSL, assurez-vous que « Utilisez SSL » soit défini à « OFF » ;
   -  changez-le pour la valeur « 80 » ;
   -  revenez aux informations de compte et sauvegardez.

Vous pouvez maintenant trouver vos contacts dans le carnet d'adresses de votre iPhone.
Si cela ne fonctionne toujours pas, consultez les guides :doc:`troubleshooting`
et `Dépannage pour Contacts & Agendas`_.

.. _Dépannage pour Contacts & Agendas: https://doc.owncloud.org/server/9.0/admin_manual/issues/index.html#troubleshooting-contacts-calendar
