.. _index:

=====================================================
ownCloud |version| Introduction du manuel utilisateur
=====================================================

**Bienvenue dans ownCloud : votre solution d'hébergement et de partage de fichiers personnelle**

ownCloud est un logiciel libre de partage et de synchronisation de documents pour tout le monde,
des particuliers utilisant la version gratuite « ownCloud Server edition », aux grandes entreprises
et fournisseurs de services utilisant « ownCloud Enterprise Subscription ». ownCloud
propose une solution de synchronisation et de partage de fichiers sûre et conforme,
sur des serveurs sous votre contrôle.

Vous pouvez partager un ou plusieurs fichiers ou dossiers sur votre ordinateur et les synchroniser 
avec votre serveur ownCloud. Placez des fichiers dans vos répertoires partagés locaux et 
ils seront immédiatement synchronisés avec le serveur et d'autres équipements utilisant le client 
de synchronisation pour ordinateur ownCloud, l'application Android ou l'application iOS app. Pour en apprendre plus sur 
les clients ownCloud pour ordinateur ou mobile, veuillez vous référer à leur manuel
respectif :

* `Client pour ordinateur ownCloud`_
* `Application ownCloud pour Android`_
* `Application ownCloud pour iOS`_ 

.. _`Client pour ordinateur ownCloud`: https://doc.owncloud.org/desktop/2.1/
.. _`Application ownCloud pour Android`: https://doc.owncloud.org/android/
.. _`Application ownCloud pour iOS`: https://doc.owncloud.org/ios/
