======================================
Les nouveautés dans ownCloud |version|
======================================

* Étiquetage des fichiers et filtrage par étiquettes (:doc:`files/access_webgui`) ;
* liste des fichiers re-partagés que vous avez partagés ;
* complétion automatique des noms d'utilisateurs et de groupes au sein des partages fédérés
  (:doc:`files/federated_cloud_sharing`) ;
* commentaires partagés sur les fichiers ;
* visionneuse vidéo intégrée.
