===================================
Guide de style des manuels ownCloud
===================================

*Ceci est un travail en cours*

Consulter le `README de la Documentation <https://github.com/owncloud/documentation/blob/master/README.rst>`_
pour des informations sur le paramétrage de l'environnement de compilation de la documentation.

Consulter `reStructuredText Primer <http://sphinx-doc.org/rest.html>`_ pour une référence complète
sur le langage de balises Sphinx/RST.

Ceci est le guide de style officiel pour les manuels ownCloud administrateur et utilisateur. 
Veuillez suivre ces conventions pour permettre une relecture plus facile et pour la cohérence
de la documentation.

Quand vous écrivez votre texte, soyez aussi littéral et spécifique que possible. Mettez-vous 
à la place de la personne qui utilise ownCloud et qui cherche des
instructions sur la réalisation d'une tâche. Ne la laissez pas deviner, mais décrivez plutôt
exactement chaque étape en indiquant sur quels boutons cliquer ou les champs de
formulaires à remplir. Donnez l'information complète. Par exemple, pour la configuration d'une
valeur de délai de temporisation, assurez-vous d'indiquer s'il s'agit de secondes ou d'une 
autre valeur. Spécifiez ``config.php`` plutôt que « le fichier de configuration ». Quand vous
décrivez les fonctionnalités de l'interface graphique d'administration, utilisez les noms de
boutons, de champs de formulaires ou de menus tels qu'ils apparaissent dans l'interface. 
Indiquer si les menus sont déroulants, s'il faut faire un clic gauche, un clic droit ou s'il
faut passer le curseur de la souris au-dessus d'un élément de l'interface.

Noms de fichiers
----------------

Utiliser des lettres en minuscules avec des traits de soulignement. Par exemple : « file_name_config.rst ».

Titres et sous-titres
---------------------

Il existe beaucoup de façons d'indiquer les titres et sous-titres. Ceci est la norme officielle
d'ownCloud. Trois niveaux suffisent. Si vous trouvez qu'il en faudrait plus, repensez
l'organisation de votre texte::

 =================
 Titre de page, h1
 =================

 Titre de niveau 2, h2
 ---------------------

 Titre de niveau 3, h3
 ^^^^^^^^^^^^^^^^^^^^^
 
Voici le rendu :

=================
Titre de page, h1
=================

Titre de niveau 2, h2
---------------------

Titre de niveau 3, h3
^^^^^^^^^^^^^^^^^^^^^

Libellés et code
----------------

Les éléments de l'interface graphique de configuration sont indiqués en gras et doivent
être décrits aussi littéralement que possible, de sorte que votre description corresponde
exactement à ce que l'utilisateur voit à l'écran. Par exemple, sur la page de listing
Utilisateur, décrivez les divers éléments comme ceci::

 Champ **Nom d'utilisateur**
 Champ **Mot de passe**
 Menu déroulant **Groupes**
 Bouton **Créer**
 Champ **Nom complet**
 Menu déroulant **Quota**
 
Voici le rendu :
 
Champ **Nom d'utilisateur**

Champ **Mot de passe**

Menu déroulant **Groupes**

Bouton **Créer**

Champ **Nom complet**

Menu déroulant **Quota**

.. figure:: users-config.png
   :alt: Listing utilisateurs et page d'administration.
   
   *Illustration 1 : Le listing des utilisateurs et la page d'administration d'ownCloud.*
   
Utiliser des doubles apostrophes inversées pour le code intégré et les exemples de commandes::
  
  ``sudo -u www-data php occ files:scan --help``
  ``maintenance:install``
  
Voici le rendu :

``sudo -u www-data php occ files:scan --help``

``maintenance:install``

Quand vous donnez des exemples d'hyperliens, utilisez des doubles apostrophes inversées plutôt 
qu'un hyperlien actif::

 ``https://exemple.com``

Images
------

Utiliser des lettres en minuscules et des tirets pour les noms d'images. Par exemple : « image-name.png ».

Les images doivent être au format « .png ». Gardez vos copies d'écrans concentrées sur les éléments
que vous décrivez. Si vous avez besoin d'une copie d'écran grande telle que la page de configuration 
dans l'administration d'ownCloud, réduisez la taille de votre navigateur Web pour l'adapter aux champs
que vous décrivez pour réduire la taille de la copie d'écran.

Toutes les images doivent avoir une balise alt, qui est une courte description de l'image, et
un numéro. Sphinx RST ne dispose pas de balise pour numéroter les images, vous devez donc utiliser
l'élément « caption ». Vous pouvez utiliser une numérotation simple telle que « Illustration 1, Illustration 2", 
ou ajouter une légende. Les légendes doivent être précédées d'une ligne vide et être en italiques, comme dans cet exemple::

  .. figure:: images/users-config.png
     :alt: Listing utilisateurs et page d'administration.
   
     *Illustration 1 : Le listing des utilisateurs et la page d'administration d'ownCloud.*

Les images doivent être placées dans un sous-répertoires du répertoire contenant votre page du
manuel. Actuellement, les manuels contiennent un seul répertoire d'images. La maintenance d'un seul
répertoire d'images est difficile et celui-ci se remplit inévitablement d'images obsolètes. À terme, le
répertoire d'images unique disparaîtra.

URL d'exemple
-------------

Utilisez ``https://exemple.com`` pour les exemples dans lesquels vous voulez inclure une URL.
